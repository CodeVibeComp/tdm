﻿using UserService.Domain.Abstractions;
using UserService.Domain.Aggregates.PostAggregate.Exceptions;

namespace UserService.Domain.Aggregates.Post;

/// <summary>
/// Агрегат поста
/// </summary>
public class PostAggregate : AggregateRoot
{
    /// <summary>
    /// Идентификатор изображения в храналище
    /// </summary>
    public string ImageName { get; set; }

    /// <summary>
    /// Описание поста
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Базовый конструктор
    /// </summary>
    public PostAggregate(Guid id) : base(id)
    {
        // Инициализация переменной для хранения имени изображения
        ImageName = null!;

        // Инициализация переменной для хранения описания изображения
        Description = null!;
    }

    /// <summary>
    /// Метод для создания агрегата поста
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <param name="imageName">Название файла изображения в хранилище</param>
    /// <param name="description">Описание поста</param>
    /// <returns>Агрегат поста</returns>
    /// <exception cref="IncorrectDescriptionException">Исключение вызывается при некорректном описании</exception>
    /// <exception cref="EmptyImageNameException">Исключение вызывается при пустом названии изображения</exception>
    public static PostAggregate Create(Guid id, string imageName, string description)
    {
        // Проверка, если длина описания больше допустимой 
        if (description.Length > SettingsPostAggregate.MaxDescLength)
            throw new IncorrectDescriptionException(SettingsPostAggregate.MinDescLength, SettingsPostAggregate.MaxDescLength);

        // Проверка, если длина описания меньше допустимой 
        if (description.Length < SettingsPostAggregate.MinDescLength)
            throw new IncorrectDescriptionException(SettingsPostAggregate.MinDescLength, SettingsPostAggregate.MaxDescLength);

        // Проверка, если название изображения пустое
        if (string.IsNullOrEmpty(imageName))
            throw new EmptyImageNameException();

        // Формируем и возвращаем пост
        return new PostAggregate(id)
        {
            // Заполняем название изображения
            ImageName = imageName,

            // Заполняем описание
            Description = description,
        };
    }
}
