﻿namespace UserService.Domain.Aggregates.PostAggregate.Exceptions;

/// <summary>
/// Исключение при пустом названии изображения
/// </summary>
public class EmptyImageNameException() :
    Exception($"ImageName is empty");