﻿namespace UserService.Domain.Aggregates.PostAggregate.Exceptions;

/// <summary>
/// Исключение при неккоректном описании
/// </summary>
/// <param name="minlength">Минимальное количество символов</param>
/// <param name="maxlength">Максимальное количество символов</param>
public class IncorrectDescriptionException
    (int minlength, int maxlength)
    : Exception($"Invalid property Description, the minimum number of characters should be {minlength}" +
               $" and the maximum number of characters should be {maxlength} characters");
