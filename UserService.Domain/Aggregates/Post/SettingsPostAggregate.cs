﻿namespace UserService.Domain.Aggregates.Post;

/// <summary>
/// Статический класс настроек агреггата поста
/// </summary>
public static class SettingsPostAggregate
{
    /// <summary>
    /// Максимальное количество символов в описании
    /// </summary>
    public static int MinDescLength { get; set; }

    /// <summary>
    /// Минимальное количество символов в описании
    /// </summary>
    public static int MaxDescLength { get; set; }
}
