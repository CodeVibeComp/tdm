using UserService.Domain.Abstractions;
using UserService.Domain.Aggregates.Organization.Exceptions;

namespace UserService.Domain.Aggregates.Organization;

/// <summary>
/// Аггрегат организации
/// </summary>
public class OrganizationAggregate : AggregateRoot
{
    /// <summary>
    /// Название организации
    /// </summary>
    public string Name { get; private init; }

    /// <summary>
    /// Инициализирует новый экземпляр аггрегата
    /// </summary>
    /// <param name="id">Идентификатор</param>
    private OrganizationAggregate(Guid id) : base(id)
    {
        Name = null!;
    }

    /// <summary>
    /// Метод для создания Организации
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <param name="name">Название организации</param>
    /// <returns>Аггрегат организации</returns>
    /// <exception cref="IncorrectOrgNameException">Исключение вызывается при некорректном имени</exception>
    public static OrganizationAggregate Create(Guid id, string name)
    {
        // Проверка корректности названия организации 
        if (name.Length < SettingsOrganizationAggregate.MinNameLength || name.Length > SettingsOrganizationAggregate.MaxNameLength)

            // Если некорректное имя выбрасываем исключение
            throw new IncorrectOrgNameException(SettingsOrganizationAggregate.MinNameLength, SettingsOrganizationAggregate.MaxNameLength);
        
        // Возвращаем аггрегат
        return new OrganizationAggregate(id)
        {
            Name = name
        };
    }
}