﻿namespace UserService.Domain.Aggregates.Organization;

/// <summary>
/// Статический класс настроек агреггата организации
/// </summary>
public static class SettingsOrganizationAggregate
{
    /// <summary>
    /// Максимальное количество символов в названии
    /// </summary>
    public static int MaxNameLength { get; set; }

    /// <summary>
    /// Минимальное количество символов в названии
    /// </summary>
    public static int MinNameLength { get; set; }
}
