namespace UserService.Domain.Aggregates.Organization.Exceptions;

/// <summary>
/// Исключение для некорректного имени
/// </summary>
/// <param name="minlength">Минимальное количество символов</param>
/// <param name="maxlength">Максимальное количество символов</param>
public class IncorrectOrgNameException(int minlength, int maxlength)
    :Exception($"Invalid property Name, the minimum number of characters should be {minlength}" +
               $" and the maximum number of characters should be {maxlength} characters");