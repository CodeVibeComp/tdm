namespace UserService.Domain.Aggregates.User.Exceptions;

/// <summary>
/// Исключение при неккоректной электронной почте
/// </summary>
public class IncorrectEmailException() : Exception("Invalid property Email. Email should be in the format example@gmail.com");
