namespace UserService.Domain.Aggregates.User.Exceptions;

/// <summary>
/// Исключение при неккоректной Фамилии
/// </summary>
/// <param name="minlength">Минимальное количество символов</param>
/// <param name="maxlength">Максимальное количество символов</param>
public class IncorrectLastNameException
    (int minlength, int maxlength)
    : Exception($"Invalid property LastName, the minimum number of characters should be {minlength}" +
               $" and the maximum number of characters should be {maxlength} characters");