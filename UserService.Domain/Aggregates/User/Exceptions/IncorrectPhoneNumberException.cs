namespace UserService.Domain.Aggregates.User.Exceptions;

/// <summary>
/// Исключение при неккоректном номере телефона
/// </summary>
public class IncorrectPhoneNumberException()
    : Exception($"Invalid property PhoneNumber. The phone number must contain 11 digits");