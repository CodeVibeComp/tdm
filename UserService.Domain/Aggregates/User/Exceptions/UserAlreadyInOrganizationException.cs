namespace UserService.Domain.Aggregates.User.Exceptions;

/// <summary>
/// Исключение при попытке добавить пользователя в уже существующую организацию
/// </summary>
/// <param name="idUser">Идентификатор пользователя</param>
/// <param name="organizationId">Индетификатор организации</param>
public class UserAlreadyInOrganizationException(Guid idUser, Guid organizationId)
    : Exception($"The user with id {idUser} is already a member of the {organizationId} organization")
{
    /// <summary>
    /// Идентификатор пользователя
    /// </summary>
    public Guid IdUser => idUser;

    /// <summary>
    /// Идентификатор организации
    /// </summary>
    public Guid OrganizationId => organizationId;
}