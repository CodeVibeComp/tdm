using UserService.Domain.Abstractions;
using UserService.Domain.Aggregates.Organization;
using UserService.Domain.Aggregates.User.Exceptions;

namespace UserService.Domain.Aggregates.User;

/// <summary>
/// Аггрегат пользователя
/// </summary>
public class UserAggregate : AggregateRoot
{
    /// <summary>
    /// Имя
    /// </summary>
    public string FirstName { get; private init; }

    /// <summary>
    /// Фамилия
    /// </summary>
    public string LastName { get; private init; }

    /// <summary>
    /// Отчество
    /// </summary>
    public string? Patronymic { get; private init; }

    /// <summary>
    /// Номер телефона
    /// </summary>
    public string PhoneNumber { get; private init; }

    /// <summary>
    /// Электронная почта
    /// </summary>
    public string Email { get; private set; }

    /// <summary>
    /// Идентификатор организации
    /// </summary>
    public Guid? OrganizationId { get; private set; }

    /// <summary>
    /// Инициализация пользователя
    /// </summary>
    /// <param name="id">Идентификатор пользователя</param>
    private UserAggregate(Guid id) : base(id)
    {
        FirstName = null!;
        LastName = null!;
        Email = null!;
        PhoneNumber = null!;
    }

    /// <summary>
    /// Инициализация пользователя по-умолчанию
    /// </summary>
    public UserAggregate() : base(Guid.Empty)
    {
        FirstName = string.Empty;
        LastName = string.Empty;
        Email = string.Empty;
        PhoneNumber = string.Empty;
    }

    public void SetToOrganization(OrganizationAggregate organization)
    {
        //Проверка состоит ли пользователь в организации
        if (OrganizationId.HasValue)
            //Если пользователь состоит в организации выбрасываем исключение
            throw new UserAlreadyInOrganizationException(Id, OrganizationId.Value);

        //Устанавливаем пользователя в организацию
        OrganizationId = organization.Id;
    }

    /// <summary>
    /// Метод для создания агрегата пользователя
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <param name="firstName">Имя</param>
    /// <param name="lastName">Фамилия</param>
    /// <param name="patronymic">Отчество</param>
    /// <param name="email">Электронная почта</param>
    /// <param name="phoneNumber">Номер телефона</param>
    /// <param name="organizationId">Идентификатор организации</param>
    /// <returns>Агрегат пользователя</returns>
    /// <exception cref="IncorrectFirstNameException">Исключение вызывается при некорректном имени</exception>
    /// <exception cref="IncorrectLastNameException">Исключение вызывается при некорректной фамилии</exception>
    /// <exception cref="IncorrectPatronymicException">Исключение вызывается при некорректном отчестве</exception>
    /// <exception cref="IncorrectEmailException">Исключение вызывается при некорректной почте</exception>
    /// <exception cref="IncorrectPhoneNumberException">Исключение вызывается при некорректном номере телефона</exception>
    public static UserAggregate Create(
        Guid id,
        string firstName,
        string lastName,
        string? patronymic,
        string email,
        string phoneNumber,
        Guid? organizationId = null)
    {
        // Проверка корректности имени
        if (firstName.Length < SettingsUserAggregate.MinFirstNameLength || firstName.Length > SettingsUserAggregate.MaxFirstNameLength)
            // Если некорректное имя выбрасываем исключение
            throw new IncorrectFirstNameException(SettingsUserAggregate.MinFirstNameLength, SettingsUserAggregate.MaxFirstNameLength);

        // Проверка корректности фамилии
        if (lastName.Length < SettingsUserAggregate.MinLastNameLength || lastName.Length > SettingsUserAggregate.MaxLastNameLength)
            // Если некорректная фамилия выбрасываем исключение
            throw new IncorrectLastNameException(SettingsUserAggregate.MinLastNameLength, SettingsUserAggregate.MaxLastNameLength);

        // Проверка корректности отчества
        if (!string.IsNullOrEmpty(patronymic) && (patronymic.Length < SettingsUserAggregate.MinPatronymicLength || patronymic.Length > SettingsUserAggregate.MaxPatronymicLength))
            // Если некорректное отчество выбрасываем исключение
            throw new IncorrectPatronymicException(SettingsUserAggregate.MinPatronymicLength, SettingsUserAggregate.MaxPatronymicLength);

        // Проверка корректности почты
        if (!SettingsUserAggregate.RegexEmail.IsMatch(email))
            // Если некорректная почта выбрасываем исключение
            throw new IncorrectEmailException();

        // Проверка корректности номера телефона
        if (!SettingsUserAggregate.RegexPhoneNumber.IsMatch(phoneNumber))
            // Если некорректное имя выбрасываем исключение
            throw new IncorrectPhoneNumberException();

        // Возвращаем аггрегат пользователя
        return new UserAggregate(id)
        {
            OrganizationId = organizationId,
            FirstName = firstName,
            LastName = lastName,
            Patronymic = patronymic,
            Email = email,
            PhoneNumber = phoneNumber
        };
    }
}