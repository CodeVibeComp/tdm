﻿using OrganizationAggregate = UserService.Domain.Aggregates.Organization.SettingsOrganizationAggregate;
using PostAggregate = UserService.Domain.Aggregates.Post.SettingsPostAggregate;
using UserAggregate = UserService.Domain.Aggregates.User.SettingsUserAggregate;

namespace UserService.App.Settings;

/// <summary>
/// Статический класс для заполнения с конфига настроек агрегатов
/// </summary>
public static class AggregatesSettings
{
    /// <summary>
    /// Метод заполнения настроек агрегатов
    /// </summary>
    /// <param name="configuration">Конфигурация приложения</param>
    public static void AddAggregatesSettings(this IServiceCollection services, IConfiguration configuration)
    {
        // Загружаем из конфига все настройки агрегатов
        var settings = configuration.GetSection("AggregatesSettings");

        // Загружаем настройки агрегата организаций
        var organizationSettings = settings.GetSection("Organization");
        
        // Загружаем настройки агрегата постов
        var postSettings = settings.GetSection("Post");

        // Загружаем настройки агрегата пользователя
        var userSettings = settings.GetSection("User");

        // Устанавливаем настройки агрегата огранизаций
        SetOrganizationSettings(organizationSettings);

        // Устанавливаем настройки агрегата поста
        SetPostSettings(postSettings);

        // Устанавливаем настройки агрегата пользователя
        SetUserSettings(userSettings);
    }

    /// <summary>
    /// Метод установления настроек агрегата огранизации
    /// </summary>
    /// <param name="section">Секция конфигурации</param>
    private static void SetOrganizationSettings(IConfigurationSection section)
    {
        // Устанавливаем максимальную длину названия организации
        OrganizationAggregate.MaxNameLength = section.GetValue<int>("MaxNameLength");

        // Устанавливаем минимальную длину названия организации
        OrganizationAggregate.MinNameLength = section.GetValue<int>("MinNameLength");
    }

    /// <summary>
    /// Метод установления настроек агрегата поста
    /// </summary>
    /// <param name="section">Секция конфигурации</param>
    private static void SetPostSettings(IConfigurationSection section)
    {
        // Устанавливаем максимальную длину описания
        PostAggregate.MaxDescLength = section.GetValue<int>("MaxDescLength");

        // Устанавливаем минимальную длину описания
        PostAggregate.MinDescLength = section.GetValue<int>("MinDescLength");
    }

    /// <summary>
    /// Метод установления настроек агрегата пользователя
    /// </summary>
    /// <param name="section">Секция конфигурации</param>
    private static void SetUserSettings(IConfigurationSection section)
    {
        // Устанавливаем максимальную длину имени
        UserAggregate.MaxFirstNameLength = section.GetValue<int>("MaxFirstNameLength");

        // Устанавливаем минимальную длину имени
        UserAggregate.MinFirstNameLength = section.GetValue<int>("MinFirstNameLength");

        // Устанавливаем максимальную длину фамилии
        UserAggregate.MaxLastNameLength = section.GetValue<int>("MaxLastNameLength");

        // Устанавливаем минимальную длину фамилии
        UserAggregate.MinLastNameLength = section.GetValue<int>("MinLastNameLength");

        // Устанавливаем максимальную длину отчества
        UserAggregate.MaxPatronymicLength = section.GetValue<int>("MaxPatronymicLength");

        // Устанавливаем минимальную длину отчества
        UserAggregate.MinPatronymicLength = section.GetValue<int>("MinPatronymicLength");
    }
}
