﻿using AutoMapper;
using UserService.Domain.Aggregates.Organization;
using UserService.Domain.Aggregates.Post;
using UserService.Domain.Aggregates.User;
using UserService.Infrastructure.Storage.Models;

namespace UserService.App.Settings;

/// <summary>
/// Класс настройки профиля AutoMapper
/// </summary>
public class AutoMappingProfile: Profile
{
    /// <summary>
    /// Правила маппинга
    /// </summary>
    public AutoMappingProfile()
    {
        // Настраиваем маппинг OrganizationModel в Organization
        CreateMap<OrganizationModel, OrganizationAggregate>();

        // Настраиваем маппинг PostModel в Post
        CreateMap<PostModel, PostAggregate>();

        // Настраиваем маппинг UserModel в User
        CreateMap<UserModel, UserAggregate>();

        // Настраиваем маппинг Organization в OrganizationModel
        CreateMap<OrganizationAggregate, OrganizationModel>();

        // Настраиваем маппинг Post в PostModel
        CreateMap<PostAggregate, PostModel>();

        // Настраиваем маппинг User в UserModel
        CreateMap<UserAggregate, UserModel>();
    }
}
