using UserService.App.Extensions;
using UserService.App.Middlewares;
using UserService.App.Seed;
using UserService.App.Settings;

var builder = WebApplication.CreateBuilder(args);

// Метод регистрирует сервис Serilog в контейнере DI 
builder.AddLoggingServices();

// Добавляем файл appsettings.json в конфигурацию
builder.Configuration.AddJsonFile("Configuration/appsettings.json");

// Метод регистрирует сервис MassTransit в контейнере DI 
builder.Services.AddMassTransitServices(builder.Configuration);

// Метод регистрирует сервис MediatR в контейнере DI
builder.Services.AddMediatorServices();

// Метод регистрирует сервис FluentValidation в контейнере DI
builder.Services.AddValidationServices();

// Метод регистрирует сервис Redis в контейнере DI
builder.Services.AddRedisServices(builder.Configuration);

// Метод регистрирует сервис Redis в контейнере DI
builder.Services.AddAutoMapperServices();

// Метод настраивает конфигурацию агрегатов
builder.Services.AddAggregatesSettings(builder.Configuration);

// Метод регистрации сервисов по работе с базой данных в контейнере DI
builder.Services.AddStoreServices(builder.Configuration);

// Метод регистрирует сервис MediatR в контейнере DI
builder.Services.AddMinioServices(builder.Configuration);

// Этот метод регистрирует службы, используемые для представлений или страниц.
builder.Services.AddControllers()
    .AddApplicationPart(UserService.Presentation.AssemblyReference.Assembly);

// Метод регистрирует сервис Swagger в контейнере DI
builder.Services.AddSwaggerGen();

var app = builder.Build();
app.UseMiddleware<ExceptionMiddleware>();

app.UseSwagger();
app.UseSwaggerUI();

app.MapControllers();

app.UseHttpsRedirection();

await app.SeedDatabaseAsync();

app.Run();