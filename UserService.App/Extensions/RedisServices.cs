﻿using UserService.Domain.Abstractions.Managers;
using UserService.Infrastructure.Storage.Managers;

namespace UserService.App.Extensions;

/// <summary>
/// Статический класс для регистрации сервиса Redis в контейнере DI
/// </summary>

public static class RedisServices
{
    /// <summary>
    /// Метод регистрирует сервис Redis в контейнере DI 
    /// </summary>
    /// <param name="services">Абстракция, которая представляет коллекцию сервисов (зависимостей),
    /// используемых в приложении.</param>
    public static void AddRedisServices(this IServiceCollection services, IConfiguration configuration)
    {
        // Получаем строку подключения к Redis
        var redisConnectionString = configuration.GetConnectionString("Redis") ?? throw new Exception("ConnectionStrings:Redis");

        // Регистрируем Redis в IServicesCollection
        services.AddStackExchangeRedisCache(options => options.Configuration = redisConnectionString);

        // Регистрируем менеджер для работы с кешем
        services.AddSingleton<ICacheManager, CacheManager>();
    }
}