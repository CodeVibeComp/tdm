﻿using Minio;
using UserService.Domain.Abstractions.Managers;
using UserService.Infrastructure.Storage.Managers;

namespace UserService.App.Extensions;

/// <summary>
/// Статический класс для регистрации сервиса MinIO в контейнере DI 
/// </summary>
public static class MinioServices
{
    /// <summary>
    /// Метод регистрирует сервис MinIO в контейнере DI 
    /// </summary>
    /// <param name="services">Абстракция, которая представляет коллекцию сервисов (зависимостей), используемых в приложении.</param>
    /// <param name="configuration">Конфигурация приложения</param>
    public static void AddMinioServices(this IServiceCollection services, IConfiguration configuration)
    {
        // Получаем строку подключения
        var minioEndpoint = configuration.GetSection("Minio:Endpoint").Value ?? throw new Exception("ConnectionStrings:Minio:Endpoint");

        // Получаем токен подключения
        var minioAccessKey = configuration.GetSection("Minio:AccessKey").Value ?? throw new Exception("ConnectionStrings:Minio:AccessKey");

        // Получаем секрет подключения
        var minioSecretKey = configuration.GetSection("Minio:SecretKey").Value ?? throw new Exception("ConnectionStrings:Minio:SecretKey");

        // Добавляем MinIO в коллекцию сервисов
        services.AddMinio(configureClient => configureClient
            .WithEndpoint(minioEndpoint, 9000)
            .WithCredentials(minioAccessKey, minioSecretKey)
            .WithSSL(false));

        // Регистрируем менеджер minIO для работы с изображениями постов
        services.AddSingleton<IPostImagesManager, PostImagesManager>();
    }
}
