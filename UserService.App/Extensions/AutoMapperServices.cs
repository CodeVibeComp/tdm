﻿using UserService.App.Settings;
using UserService.Infrastructure.Storage.Mappers.AggregateMappers;
using UserService.Infrastructure.Storage.Mappers.ModelMappers;

namespace UserService.App.Extensions;

/// <summary>
/// Статический класс для регистрации сервиса AutoMapper в контейнере DI 
/// </summary>
public static class AutoMapperServices
{
    /// <summary>
    /// Метод регистрирует сервис AutoMapper в контейнере DI
    /// </summary>
    /// <param name="services">Абстрактная коллекция зависимостей</param>
    public static void AddAutoMapperServices(this IServiceCollection services)
    {
        // Регистрируем профиль автомаппера
        services.AddAutoMapper(typeof(AutoMappingProfile));
    }
}
