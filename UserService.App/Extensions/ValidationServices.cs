using FluentValidation;
using UserService.Presentation.Contracts.Post;
using UserService.Presentation.Contracts.Post.Validators;
using UserService.Presentation.Contracts.User;
using UserService.Presentation.Contracts.User.Validators;

namespace UserService.App.Extensions;

/// <summary>
/// Статический класс для регистрации сервиса FluentValidation в контейнере DI 
/// </summary>
public static class ValidationServices
{
    /// <summary>
    /// Метод регистрирует сервис FluentValidation в контейнере DI 
    /// </summary>
    /// <param name="services">Абстракция, которая представляет коллекцию сервисов (зависимостей),
    /// используемых в приложении.</param>
    public static void AddValidationServices(this IServiceCollection services)
    {
        // Регистрируем валидатор модели добавления пользователя в организацию
        services.AddScoped<IValidator<AddUserToOrganizationInputModel>, AddUserToOrganizationInputModelValidator>();

        // Регистрируем валидатор модели обновления поста
        services.AddScoped<IValidator<UpdatePostInputModel>, UpdatePostInputModelValidator>();
    }
}