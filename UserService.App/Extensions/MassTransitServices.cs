using MassTransit;
using RabbitMQ.Client;
using Shared.Models;
using UserService.Infrastructure.Bus.Consumers;
using UserService.Infrastructure.Bus.Consumers.User;

namespace UserService.App.Extensions;

/// <summary>
/// Статический класс для регистрации сервиса MassTransit в контейнере DI 
/// </summary>
public static class MassTransitServices
{
    /// <summary>
    /// Метод регистрирует сервис MassTransit в контейнере DI 
    /// </summary>
    /// <param name="services">Абстракция, которая представляет коллекцию сервисов (зависимостей),
    /// используемых в приложении.</param>
    /// <param name="configuration">Интерфейс, предоставляющий доступ к конфигурации приложения.</param>
    public static void AddMassTransitServices(this IServiceCollection services, IConfiguration configuration)
    {
        // Получаем строку подключения к RabbitMq
        var rmq = configuration.GetConnectionString("RabbitMq") ?? throw new Exception("ConnectionStrings:Rabbit");

        // Конфигурируем сервис MassTransit
        services.AddMassTransit(x =>
        {
            // Добавление потребителя для обработки сообщений о созданных пользователях
            x.AddConsumer<UserCreatedConsumer>();

            // Добавление потребителя для обработки сообщений о созданных пользователях с помощью direct exchange
            x.AddConsumer<UserCreatedByDirectConsumer>();

            // Добавление потребителя для обработки сообщений о созданных пользователях с помощью headers exchange
            x.AddConsumer<UserCreatedByHeadersConsumer>();

            // Добавление потребителя для обработки сообщений о созданных пользователях с помощью topic exchange
            x.AddConsumer<UserCreatedByTopicConsumer>();

            // Добавление потребителя для обработки сообщений о созданных организациях
            x.AddConsumer<OrganizationCreatedConsumer>();

            // Добавление потребителя для обработки сообщений о созданных постах
            x.AddConsumer<PostCreatedConsumer>();

            x.UsingRabbitMq((context, cfg) =>
            {
                //Настройка подключения к RabbitMq
                cfg.Host(rmq);

                // Настраиваем конечную точку приема сообщений для созданных пользователей
                cfg.ReceiveEndpoint("AddUserByFanout", e =>
                {
                    // Установка потребителя для обработки сообщений типа UserCreated
                    e.Consumer<UserCreatedConsumer>(context);

                    // Отключение автоматической настройки топологии обмена сообщениями
                    e.ConfigureConsumeTopology = false;

                    // Биндинг очереди к обменнику с типом fanout для сообщений типа UserCreated
                    e.Bind<UserCreated>(c => { c.ExchangeType = ExchangeType.Fanout; });
                });

                // Настраиваем конечную точку приема сообщений для созданных организаций
                cfg.ReceiveEndpoint("AddOrganizationByFanout", e =>
                {
                    // Установка потребителя для обработки сообщений типа OrganizationCreated
                    e.Consumer<OrganizationCreatedConsumer>(context);

                    // Отключение автоматической настройки топологии обмена сообщениями
                    e.ConfigureConsumeTopology = false;

                    // Биндинг очереди к обменнику с типом fanout для сообщений типа OrganizationCreated
                    e.Bind<OrganizationCreated>(c => { c.ExchangeType = ExchangeType.Fanout; });
                });

                // Настраиваем конечную точку приема сообщений для постов
                cfg.ReceiveEndpoint("AddPostByFanout", e =>
                {
                    // Установка потребителя для обработки сообщений типа PostCreated
                    e.Consumer<PostCreatedConsumer>(context);

                    // Отключение автоматической настройки топологии обмена сообщениями
                    e.ConfigureConsumeTopology = false;

                    // Биндинг очереди к обменнику с типом fanout для сообщений типа PostCreated
                    e.Bind<PostCreated>(c => { c.ExchangeType = ExchangeType.Fanout; });
                });

                // Настраиваем конечную точку приема сообщений для созданных пользователей (тип обмена Direct)
                cfg.ReceiveEndpoint("AddUserByDirect", e =>
                {
                    // Установка потребителя для обработки сообщений
                    e.Consumer<UserCreatedByDirectConsumer>(context);
                    // Отключение автоматической настройки топологии потребления
                    e.ConfigureConsumeTopology = false;
                    // Связывание очереди с обменом
                    e.Bind<UserCreatedByDirect>(c =>
                    {
                        // Тип обмена - direct
                        c.ExchangeType = ExchangeType.Direct;
                        // Ключ маршрутизации
                        c.RoutingKey = "user-created";
                    });
                });

                // Настраиваем конечную точку приема сообщений для созданных пользователей (тип обмена topic)
                cfg.ReceiveEndpoint(("AddUserByTopic"), e =>
                {
                    // Установка потребителя для обработки сообщений
                    e.Consumer<UserCreatedByTopicConsumer>(context);
                    // Отключение автоматической настройки топологии потребления
                    e.ConfigureConsumeTopology = false;
                    // Связывание очереди с обменом
                    e.Bind<UserCreatedByTopic>(c =>
                    {
                        // Тип обмена - topic
                        c.ExchangeType = ExchangeType.Topic;
                        // Ключ маршрутизации с использованием шаблона
                        c.RoutingKey = "user.*";
                    });
                });

                // Настраиваем конечную точку приема сообщений для созданных пользователей (тип обмена headers)
                cfg.ReceiveEndpoint(("AddUserByHeader"), e =>
                {
                    // Установка потребителя для обработки сообщений
                    e.Consumer<UserCreatedByHeadersConsumer>(context);
                    // Отключение автоматической настройки топологии потребления
                    e.ConfigureConsumeTopology = false;
                    // Связывание очереди с обменом
                    e.Bind<UserCreatedByHeaders>(c =>
                    {
                        // Тип обмена - headers
                        c.ExchangeType = ExchangeType.Headers;
                        // Установка аргументов привязки для фильтрации сообщений
                        c.SetBindingArgument("x-match", "all");
                        c.SetBindingArgument("action", "create");
                        c.SetBindingArgument("entity", "user");
                    });
                });
            });
        });
    }
}