using Microsoft.EntityFrameworkCore;
using UserService.Infrastructure.Storage.Context;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.Abstractions.UnitOfWork;
using UserService.Infrastructure.Storage.Repositories;
using UserService.Infrastructure.Storage.UnitOfWork;

namespace UserService.App.Extensions;

/// <summary>
/// Статический класс для регистрации сервисов по работе с базой данных в контейнере DI 
/// </summary>
public static class StoreServices
{
    /// <summary>
    /// Метод регистрации сервисов по работе с базой данных в контейнере DI 
    /// </summary>
    /// <param name="services">Коллекция сервисов DI</param>
    /// <param name="configuration">Конфигурация приложения</param>
    public static void AddStoreServices(this IServiceCollection services, IConfiguration configuration)
    {
        // Получаем строку подключения к базе данных
        var connectionString = configuration.GetConnectionString("DefaultConnection") ??
                               throw new Exception("ConnectionStrings:DefaultConnection");

        // Регистрируем контекст базы данных
        services.AddDbContext<ApplicationDbContext>(config =>
        {
            //указываем провайдера базы данных со строкой подключения
            config.UseNpgsql(connectionString);
            config.EnableSensitiveDataLogging();
        });

        // Регистрируем репозиторий для модели пользователя
        services.AddScoped<IUserRepository, UserRepository>();

        // Регистрируем репозиторий для модели организации
        services.AddScoped<IOrganizationRepository, OrganizationRepository>();

        // Регистрируем репозиторий для модели поста
        services.AddScoped<IPostRepository, PostRepository>();
        
        // Регистрируем объект работы с репозиториями моделей
        services.AddScoped<IUnitOfWork, UnitOfWork>();
    }
}