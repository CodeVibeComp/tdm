using MediatR;

namespace RequestManager.Application.Abstractions.Commands;

/// <summary>
/// Команда отправки сообщения (добавление пользователя) в обменник с типом Fanout
/// </summary>
public class AddUserByDirectCommand: IRequest
{
    /// <summary>
    /// Имя
    /// </summary>
    public required string FirstName { get; init; }

    /// <summary>
    /// Фамилия
    /// </summary>
    public required string LastName { get; init; }

    /// <summary>
    /// Отчество
    /// </summary>
    public string? Patronymic { get; init; } 

    /// <summary>
    /// Номер телефона
    /// </summary>
    public required string PhoneNumber { get; init; } 

    /// <summary>
    /// Почта
    /// </summary>
    public required string Email { get; init; } 
}