﻿using MediatR;
using Microsoft.AspNetCore.Http;

namespace RequestManager.Application.Abstractions.Commands;

/// <summary>
/// Команда отправки сообщения (добавление поста) в обменник с типом Fanout
/// </summary>
public class AddPostCommand : IRequest
{
    /// <summary>
    /// Название изображения
    /// </summary>
    public required string ImageName { get; init; }

    /// <summary>
    /// Содержимое изображения
    /// </summary>
    public required byte[] ImageContent { get; init; }

    /// <summary>
    /// Описание поста
    /// </summary>
    public required string Description { get; init; }
}