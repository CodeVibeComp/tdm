using MediatR;

namespace UserService.Application.Abstractions.Commands;

/// <summary>
/// Команда для добавления организации
/// </summary>
public class AddOrganizationCommand : IRequest
{
    /// <summary>
    /// Название организации
    /// </summary>
    public required string Name { get; init; }
}