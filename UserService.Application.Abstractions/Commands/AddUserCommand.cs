using MediatR;

namespace UserService.Application.Abstractions.Commands;
/// <summary>
/// Команда для добавления пользователя
/// </summary>
public class AddUserCommand : IRequest
{
    /// <summary>
    /// Имя
    /// </summary>
    public required string FirstName { get; init; }

    /// <summary>
    /// Фамилия
    /// </summary>
    public required string LastName { get; init; }

    /// <summary>
    /// Отчество
    /// </summary>
    public string? Patronymic { get; init; }

    /// <summary>
    /// Номер телефона
    /// </summary>
    public required string PhoneNumber { get; init; }

    /// <summary>
    /// Почта
    /// </summary>
    public required string Email { get; init; }
}