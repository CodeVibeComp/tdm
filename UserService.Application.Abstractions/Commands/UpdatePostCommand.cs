﻿using MediatR;

namespace UserService.Application.Abstractions.Commands;

/// <summary>
/// Команда для обновления поста
/// </summary>
public class UpdatePostCommand : IRequest
{
    /// <summary>
    /// Индитификатор поста
    /// </summary>
    public required Guid Id { get; init; }

    /// <summary>
    /// Название изображения
    /// </summary>
    public required string ImageName { get; init; }

    /// <summary>
    /// Содержимое изображения
    /// </summary>
    public required byte[] ImageContent { get; init; }

    /// <summary>
    /// Описание поста
    /// </summary>
    public required string Description { get; init; }
}
