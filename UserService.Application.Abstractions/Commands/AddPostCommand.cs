﻿using MediatR;

namespace UserService.Application.Abstractions.Commands;

/// <summary>
/// Команда для добавления поста
/// </summary>
public class AddPostCommand: IRequest
{
    /// <summary>
    /// Название изображения
    /// </summary>
    public required string ImageName { get; init; }

    /// <summary>
    /// Содержимое изображения
    /// </summary>
    public required byte[] ImageContent { get; init; }

    /// <summary>
    /// Описание поста
    /// </summary>
    public required string Description { get; init; }
}
