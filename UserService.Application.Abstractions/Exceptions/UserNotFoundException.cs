namespace UserService.Application.Abstractions.Exceptions;

/// <summary>
/// Исключение при отсутствии пользователя
/// </summary>
/// <param name="id"></param>
public class UserNotFoundException(Guid id) : Exception($"User with id {id} not found")
{
    /// <summary>
    /// Идентификатор пользователя
    /// </summary>
    public Guid Id = id;
}