﻿namespace UserService.Application.Abstractions.Exceptions;

/// <summary>
/// Исключение при отсутствии записи в кеше
/// </summary>
/// <param name="key">Идентификатор организации</param>
public class CacheNotFoundException(string key) : Exception($"Cache record with key {key} not found")
{
    /// <summary>
    /// Ключ кеша
    /// </summary>
    public string Key = key;
}
