﻿using MediatR;
using UserService.Domain.Aggregates.Post;
using UserService.Domain.Aggregates.PostAggregate;

namespace UserService.Application.Abstractions.Queries;

/// <summary>
/// Запрос для получения постов
/// </summary>
public class GetPostsQuery : IRequest<ICollection<PostAggregate>>
{
    /// <summary>
    /// Количество записей которые нужно пропустить
    /// </summary>
    public required int Skip { get; init; }

    /// <summary>
    /// Количество записей которые нужно взять
    /// </summary>
    public required int Take { get; init; }
}