﻿using MediatR;

namespace UserService.Application.Abstractions.Queries;

/// <summary>
/// Запрос для получения изображение поста
/// </summary>
public class GetPostImageUrlQuery : IRequest<string>
{
    /// <summary>
    /// Название изображения в хранилище
    /// </summary>
    public required string ImageName { get; init; }
}