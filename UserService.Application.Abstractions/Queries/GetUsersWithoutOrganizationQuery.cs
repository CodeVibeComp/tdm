using MediatR;
using UserService.Domain.Aggregates.User;

namespace UserService.Application.Abstractions.Queries;

/// <summary>
/// Запрос на получение пользователей, которые не состоят не в одной организации
/// </summary>
public class GetUsersWithoutOrganizationQuery : IRequest<ICollection<UserAggregate>>
{
    /// <summary>
    /// Количество записей которые нужно пропустить
    /// </summary>
    public required int Skip { get; init; }

    /// <summary>
    /// Количество записей которые нужно взять
    /// </summary>
    public required int Take { get; init; }
}