using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using RequestManager.Application.Abstractions.Commands;
using RequestManager.Presentation.Contracts.User;
using RequestManager.Presentation.Contracts.User.Validators;
using RequestManager.Presentation.Controllers;

namespace RequestManager.Tests;

/// <summary>
/// Тест работоспособности контроллера добавления пользователей
/// </summary>
[TestFixture]
public class UserControllerTests
{
    // Контроллер пользователей
    private UserController _controller = null!;

    // Мок интерфейса IMediator
    private Mock<IMediator> _mediatorMock = null!;

    /// <summary>
    /// Конструктор, вызываемый перед каждым тестом
    /// </summary>
    [SetUp]
    public void SetUp()
    {
        // Создание мока для интерфейса IMediator
        _mediatorMock = new Mock<IMediator>();

        // Настройка мока, что добавление пользователя в базу возвращает успешный результат
        _mediatorMock.Setup(x => x.Send(It.IsAny<AddUserCommand>(), CancellationToken.None))
            .Returns(Task.CompletedTask);

        // Создание валидатора добавления поста
        var validator = new AddUserInputModelValidator();

        // Инициализация контроллера с параметрами
        _controller = new UserController(_mediatorMock.Object, validator);
    }

    /// <summary>
    /// Валидный тест на добавление пользователя
    /// </summary>
    [Test]
    public async Task AddUser_WithValidModel_ReturnsOk()
    {
        // Создаем модель добавляемого пользователя
        var model = new AddUserInputModel
        {
            Email = "zvg@yandex.com",
            FirstName = "Максим",
            LastName = "Завгородний",
            Patronymic = "Валерьевич",
            PhoneNumber = "+79586745866",
        };

        // Загружаем модель в контроллер для добавления пользователя
        var result = await _controller.AddUserByFanout(model, CancellationToken.None) as OkResult;

        // Ожидаем, что ответ не пустой
        Assert.That(result, Is.Not.Null);

        // Ожидаем, что ответ будет успешным (200)
        Assert.That(result!.StatusCode, Is.EqualTo(200));
    }

    /// <summary>
    /// Инвалидный тест на добавление пользователя
    /// </summary>
    /// <param name="firstName">Имя</param>
    /// <param name="lastName">Фамилия</param>
    /// <param name="phone">Номер телефона</param>
    /// <param name="email">Электронная почта</param>
    /// <param name="patronymic">Отчество</param>
    [Test]
    [TestCase("Ма", "Завгородний", "+79586745866", "zvg@yandex.com", "Валерьевич")]
    [TestCase("Максим", "Завгородний", "745866", "zvg@yandex.com", "Валерьевич")]
    [TestCase("Максим", "Завгородний", "+79586745866", "zvg", "Валерьевич")]
    [TestCase("Максим", "За", "+79586745866", "zvg@yandex.com", "Валерьевич")]
    [TestCase("Максим", "Завгородний", "+79586745866", "zvg@yandex.com", "Ва")]
    [TestCase(null, "Завгородний", null, "zvg@yandex.com", "Ва")]
    [TestCase("Максим", "Завгородний", "+79586745866", "zvg@yandex.com", "Ва")]
    [TestCase("Максим", "", "", "zvg@yandex", "Валерьевич")]
    public async Task AddUser_WithInvalidModel_ReturnsBadRequest(string firstName, string lastName, string phone, string email,
        string patronymic)
    {
        // Создаем модель добавляемого пользователя
        var model = new AddUserInputModel{
            Email = email,
            FirstName = firstName,
            LastName = lastName,
            Patronymic = patronymic,
            PhoneNumber = phone
        };

        // Загружаем модель в контроллер для добавления пользователя
        var result = await _controller.AddUserByFanout(model, CancellationToken.None) as BadRequestObjectResult;

        // Ожидаем, что результат будет не пустым
        Assert.That(result, Is.Not.Null);

        // Ожидаем, что результат будет плохим (400 код ответа)
        Assert.That(result!.StatusCode, Is.EqualTo(400));
    }
}