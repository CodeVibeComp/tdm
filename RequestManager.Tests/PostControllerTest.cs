﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using RequestManager.App.Settings;
using RequestManager.Application.Abstractions.Commands;
using RequestManager.Presentation.Contracts.Post;
using RequestManager.Presentation.Contracts.Post.Validators;
using RequestManager.Presentation.Controllers;

namespace RequestManager.Tests;

/// <summary>
/// Тест работоспособности контроллера добавления постов
/// </summary>
[TestFixture]
public class PostControllerTest
{
    // Контроллер постов
    private PostController _controller = null!;

    // Мок интерфейса IMediator
    private Mock<IMediator> _mediatorMock = null!;

    /// <summary>
    /// Конструктор, вызываемый перед каждым тестом
    /// </summary>
    [SetUp]
    public void SetUp()
    {
        // Создание мока для интерфейса IMediator
        _mediatorMock = new Mock<IMediator>();

        // Настройка, что добавления поста в базу возвращает успешный результат
        _mediatorMock.Setup(x => x.Send(It.IsAny<AddPostCommand>(), CancellationToken.None))
            .Returns(Task.CompletedTask);

        // Подключение реализации IConfiguration
        var configuration = new ConfigurationBuilder()
            .AddJsonFile("Configuration/appsettings.json")
            .Build();

        // Создание сервис-коллекции
        var services = new ServiceCollection();

        // Добавление настроек валидации
        services.AddValidationSettings(configuration);

        // Создание валидатора добавления поста
        var validator = new AddPostInputModelValidator();

        // Создания контроллера постов с параметрами
        _controller = new PostController(_mediatorMock.Object, validator);
    }

    /// <summary>
    /// Валидный тест на добавление поста
    /// </summary>
    /// <param name="imageName">Название загружаемого изображения</param>
    [Test]
    [TestCase(".png")]
    [TestCase(".jpg")]
    [TestCase(".bmp")]
    [TestCase(".jpeg")]
    public async Task AddPost_WithValidModel_ReturnsOk(string imageName)
    {
        // Создали заглушку содержимого обновляемого файла
        var stream = new MemoryStream([1]);

        // Создали загружаемую в контроллер модель поста
        var model = new AddPostInputModel
        {
            Image = new FormFile(stream, 0, 1, "image", "image.png"),
            Description = "Мой новый пост",
        };

        // Передали модель в контроллер
        var result = await _controller.AddPost(model, CancellationToken.None) as OkResult;

        // Проверяем, что результат не пустой
        Assert.That(result, Is.Not.Null);

        // Проверяем, что контроллер вернул успешный (200) ответ
        Assert.That(result!.StatusCode, Is.EqualTo(200));
    }

    /// <summary>
    /// Инвалидный тест на добавление поста с неверным форматом изображения
    /// </summary>
    /// <param name="imageName">Название загружаемого изображения</param>
    [Test]
    [TestCase(".gif")]
    [TestCase(".docx")]
    [TestCase(".exe")]
    [TestCase(".pdf")]
    public async Task AddPost_WithInvalidImageFormat_ReturnsBadRequest(string imageName)
    {
        // Создали заглушку содержимого обновляемого файла
        var stream = new MemoryStream([1]);

        // Создали загружаемую в контроллер модель поста
        var model = new AddPostInputModel
        {
            Image = new FormFile(stream, 0, 1, imageName, imageName),
            Description = "Мой новый пост",
        };

        // Передали модель в контроллер
        var result = await _controller.AddPost(model, CancellationToken.None) as BadRequestObjectResult;

        // Проверяем, что ответ не пустой
        Assert.That(result, Is.Not.Null);

        // Проверяем, что контроллер вернул плохой (400) ответ
        Assert.That(result!.StatusCode, Is.EqualTo(400));
    }
}