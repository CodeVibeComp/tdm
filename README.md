## inter/TestDockerMassTransitMediatR-2

## Начало работы

Ссылки для скачивания:

URL-адрес клонирования SSH:  [git@gitlab.com:CodeVibeComp/tdm.git](git@gitlab.com:CodeVibeComp/tdm.git)

URL-адрес клонирования HTTPS:  
[https://gitlab.com/CodeVibeComp/tdm.git](https://gitlab.com/CodeVibeComp/tdm.git)

Эти инструкции позволят вам запустить копию проекта на вашем локальном компьютере для целей разработки и тестирования.

## Предварительные условия

Для развертывания проекта необходимо установить ПО Docker и дополнение к нему Docker Compose.

Инструкцию по установке можно посмотреть по ссылке:  [https://docs.docker.com](https://docs.docker.com/).

## Развертывание

Для запуска приложение необходимо перейти в корневую папку проекта и выполнить в терминале команду:

    docker compose up -d

После чего все проекты будут развернуты в среде Docker.
Сервис принятия запросов будет доступен по адресу:  [http://localhost:5000](http://localhost:5000/).

Сервис обработки запросов будет доступен по адресу:  [http://localhost:5001](http://localhost:5001/).

Для этих сервисов предусмотрена спецификация OpenAPI по пути  /swagger/index.html.

**База данных PostgresSQL:**

    Хост: postgres:5432
    Логин: pguser
    Пароль: pgpassword
    Имя базы данных: testDb

    Команда для запуска контейнера с базой данных:
    docker run -d --name testDbContainer -e POSTGRES_USER=pguser -e POSTGRES_PASSWORD=pgpassword -e POSTGRES_DB=testDb -p 5444:5432 postgres:14.3-alpine


**Брокер сообщений RabbitMQ:**

    Хост: rabbitmq:5672
    Веб-интерфейс: http://localhost:15672
    Логин: rmuser
    Пароль: rmpassword

    Команда для запуска контейнера с RabbitMq:

    docker run -d --name testRabbitMqContainer -e RABBITMQ_DEFAULT_USER=rmuser -e RABBITMQ_DEFAULT_PASS=rmpassword -p 15672:15672 rabbitmq:3.12.10-management


**База данных Redis**

    Хост: redis:6379

**База данных ElasticSearch**

    Хост: http://elasticsearch:9200
    Веб-интерфейс: http://localhost:5601/app/kibana

**Объектное хранилище MinIO**

    Эндпоинт: minio
    Веб-интерфейс: http://localhost:9090/
    Root-логин: minio_admin
    Root-пароль: minio_pass

    Для работы нужно перейти по веб-интерфейсу и получить AccessKey и SecretKey соответственно.


## О проекте
Данный проект был создан для изучения:
- Чистая архитектура
- CQRS
- Rabbit Mq
- DTO 
- Domain-Driven Design
- Паттерн репозиторий 

Cтек: ASP NET, MediatR, MassTransit, EntityFramework, FluentValidation, Docker, Postgres, ElasticSearch, Redis, MinIO

Сервис RequestManager обрабатывает POST запросы  на создание пользователя/организации через разные  типы обменников , присутствует простая валидация.  Далее сообщения (об создании пользователя/организации) отправляются в RabbitMq в соответствии с выбранным типом обменника.

Cервис UserService - получает сообщения от RabbitMq и обрабатывает их в соответствии с типом обменника. Обработка представляет собой, сохранение полученных сообщений в базе данных и логирование их в консоли. Так же это сервис предоставляет API с возможностями:

- Получить пользователей, который входят в организацию, вывод постранично (постраничный вывод)
- Получить пользователей, которые не входят не в какую организацию (постраничный вывод) . 
- Добавить определенного пользователя в организацию. 
- Получить список всех организаций (постраничный вывод)
- Получить список всех постов (постраничный вывод)
- Обновить содержимое поста


## Структура
Данное решение было спроектировано в рамках Чистой архитектуры  
```
Test - Корневая папка решения
├── RequestManager - сервис 
│   ├── Application - слой приложения
│   ├── Presentation - слой представления
│   ├── Tests - Юнит тесты сервиса RequestManager 
│   ├── RequestManager.App - веб приложение 
├── UserService - сервис
│   ├── Application - слой приложения
│   ├── Domain - слой домена
│   ├── Infrastructure - слой инфраструктуры
│   ├── Presentation - слой представления
│   ├── Tests - юнит тесты 
│   ├── UserService.App - веб приложение 
├── Shared - библиотека классов, содержит Эвенты сообщений для работы c MassTransit
```
**Слои сервиса RequestManager**

Слой Application содержит: 
- Команды и запросы к MediatoR
- Producers
> Producers это компоненты, которые отвечают за отправку сообщений в шину сообщений RabbitMQ. Они создают и публикуют сообщения в определенный обменник (Exchange)

Слой Presentation содержит
- Контроллеры
- Валидаторы 
- Модельные классы ViewModel и InputModel 

 **Слои сервиса UserService**
 
Слой Application содержит:
-  Команды и запросы к MediatoR

Слой Domain содержит:

- Агрегаты 
- Интерфейсы репозиториев 
- Интерфейсы менеджеров

Слой Infrastructure содержит 

- Реализацию репозиториев
- Контекст базы данных
- Модели таблиц базы данных
- Мапперы
- Миграции
- Consumers 

> Consumers это компоненты, которые подписываются на сообщения в шине
> сообщений RabbitMQ и обрабатывают их.