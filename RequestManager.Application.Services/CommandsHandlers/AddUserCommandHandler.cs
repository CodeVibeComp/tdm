using MassTransit;
using MediatR;
using RequestManager.Application.Abstractions.Commands;
using Microsoft.Extensions.Logging;
using Shared.Models;

namespace RequestManager.Application.Services.CommandsHandlers;

/// <summary>
/// Обработчик команды для отправки сообщения (добавление пользователя) в обменник с типом Fanout 
/// </summary>
public class AddUserCommandHandler(
    ILogger<AddUserCommandHandler> logger, 
    IPublishEndpoint publishEndpoint)
    : IRequestHandler<AddUserCommand>
{
    /// <summary>
    /// Метод обработчик команды по добавлению пользователя
    /// </summary>
    /// <param name="request">Команда для добавления пользователя</param>
    /// <param name="cancellationToken">Токен для отмены операции</param>
    /// <returns>Task</returns>
    public Task Handle(AddUserCommand request, CancellationToken cancellationToken)
    {
        //Логируем отправку
        logger.LogInformation("(Fanout) Отправка данных пользователя по шине: {LastName} {FirstName}",
            request.LastName,
            request.FirstName);

        //Публикуем сообщение
        publishEndpoint.Publish(new UserCreated
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Patronymic = request.Patronymic,
                PhoneNumber = request.PhoneNumber
            }
        );

        //возвращаем удачный результат задачи
        return Task.CompletedTask;
    }
}