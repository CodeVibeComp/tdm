using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using RequestManager.Application.Abstractions.Commands;
using Shared.Models;

namespace RequestManager.Application.Services.CommandsHandlers;

/// <summary>
/// Обработчик команды для отправки сообщения (добавление пользователя) в обменник с типом Headers 
/// </summary>
/// <param name="producer">Интерфейс генерирующий отправку сообщения по шине</param>
public class AddUserByHeadersCommandHandler(
    IPublishEndpoint publishEndpoint,
    ILogger<AddUserByHeadersCommandHandler> logger)
    : IRequestHandler<AddUserByHeadersCommand>
{
    /// <summary>
    /// Метод обработчик команды по добавлению пользователя
    /// </summary>
    /// <param name="request">Команда для добавления пользователя</param>
    /// <param name="cancellationToken">Токен для отмены операции</param>
    /// <returns>Task</returns>
    public Task Handle(AddUserByHeadersCommand request, CancellationToken cancellationToken)
    {
        //Логируем отправку
        logger.LogInformation("(Headers) Отправка данных пользователя по шине: {LastName} {FirstName}",
            request.LastName,
            request.FirstName);
        //Публикуем сообщение
        publishEndpoint.Publish(new UserCreatedByHeaders
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                PhoneNumber = request.PhoneNumber,
                Email = request.Email,
                Patronymic = request.Patronymic
            },
            context =>
            {
                context.Headers.Set("action", "create");
                context.Headers.Set("entity", "user");
            });

        //возвращаем удачный результат задачи
        return Task.CompletedTask;
    }
}