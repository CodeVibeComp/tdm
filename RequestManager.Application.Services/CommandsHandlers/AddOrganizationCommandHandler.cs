using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using RequestManager.Application.Abstractions.Commands;
using Shared.Models;

namespace RequestManager.Application.Services.CommandsHandlers;

/// <summary>
/// Обработчик команды для отправки сообщения (добавление организации) в обменник с типом Fanout 
/// </summary>
public class AddOrganizationCommandHandler : IRequestHandler<AddOrganizationCommand>
{
    /// <summary>
    /// Конечная точка публикации
    /// </summary>
    private readonly IPublishEndpoint _publishEndpoint;
    
    /// <summary>
    /// Логгер
    /// </summary>
    private readonly ILogger<AddOrganizationCommandHandler> _logger;

    /// <summary>
    /// Обработчик команды для отправки сообщения (добавление организации) в обменник с типом Fanout 
    /// </summary>
    /// <param name="publishEndpoint">Конечная точка публикации</param>
    /// <param name="logger">Логгер</param>
    public AddOrganizationCommandHandler(IPublishEndpoint publishEndpoint,
        ILogger<AddOrganizationCommandHandler> logger)
    {
        _publishEndpoint = publishEndpoint;
        _logger = logger;
    }

    /// <summary>
    /// Метод обработчик команды по добавлению пользователя
    /// </summary>
    /// <param name="request">Команда для добавления пользователя</param>
    /// <param name="cancellationToken">Токен для отмены операции</param>
    /// <returns>Task</returns>
    public Task Handle(AddOrganizationCommand request, CancellationToken cancellationToken)
    {
        //Логгируем отправку
        _logger.LogInformation("Отправка данных организации по шине: {Name} ",
            request.Name);

        //Публикуем сообщение
        _publishEndpoint.Publish(new OrganizationCreated{Name = request.Name}, cancellationToken);
        
        //возвращаем удачный результат задачи
        return Task.CompletedTask;
    }
}