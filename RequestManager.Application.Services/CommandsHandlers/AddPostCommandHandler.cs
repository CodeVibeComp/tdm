﻿using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using RequestManager.Application.Abstractions.Commands;
using Shared.Models;

namespace RequestManager.Application.Services.CommandsHandlers;

/// <summary>
/// Обработчик команды для отправки сообщения (добавление поста) в обменник с типом Fanout 
/// </summary>
/// <param name="producer">Интерфейс генерирующий отправку сообщения по шине</param>
public class AddPostCommandHandler(
    IPublishEndpoint publishEndpoint,
    ILogger<AddOrganizationCommandHandler> logger)
    : IRequestHandler<AddPostCommand>
{
    /// <summary>
    /// Метод обработчик команды по добавлению поста
    /// </summary>
    /// <param name="request">Команда для добавления поста</param>
    /// <param name="cancellationToken">Токен для отмены операции</param>
    /// <returns>Task</returns>
    public Task Handle(AddPostCommand request, CancellationToken cancellationToken)
    {
        // Логируем отправку
        logger.LogInformation("Отправка данных организации по шине: {Name} ",
            request.ImageName);

        // Публикуем сообщение
        publishEndpoint.Publish(new PostCreated
        {
            ImageName = request.ImageName,
            ImageContent = request.ImageContent,
            Description = request.Description,
        });

        // Возвращаем выполненный результат задачи
        return Task.CompletedTask;
    }
}