﻿using MediatR;
using UserService.Application.Abstractions.Queries;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.Abstractions.UnitOfWork;
using UserService.Domain.Aggregates.Post;

namespace UserService.Application.Services.QueriesHandlers;

/// <summary>
/// Обработчик запроса для получения постов
/// </summary>
/// <param name="unitOfWork">Интерфейс единицы работы с репозиториями</param>
public class GetPostsQueryHandler(IUnitOfWork unitOfWork) : IRequestHandler<GetPostsQuery, ICollection<PostAggregate>>
{
    /// <summary>
    /// Обработчик запроса
    /// </summary>
    /// <param name="request">Запрос для получения постов</param>
    /// <param name="cancellationToken">Распространяет уведомление о том, что операции следует отменить.</param>
    public Task<ICollection<PostAggregate>> Handle(GetPostsQuery request, CancellationToken cancellationToken)
    {
        // Поиск постов и возвращение коллекции 
        return unitOfWork.PostRepository.FindAsync(request.Skip, request.Take);
    }
}