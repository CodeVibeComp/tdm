﻿using MediatR;
using UserService.Application.Abstractions.Queries;
using UserService.Domain.Abstractions.Managers;

namespace UserService.Application.Services.QueriesHandlers;

/// <summary>
/// Обработчик запроса для получения ссылки изображения поста
/// </summary>
/// <param name="imagesManager">Интерфейс менеджера изображений постов</param>
public class GetPostImageUrlQueryHandler(IPostImagesManager imagesManager) : IRequestHandler<GetPostImageUrlQuery, string>
{
    /// <summary>
    /// Обработчик запроса
    /// </summary>
    /// <param name="request">Запрос для получения ссылки на изображение поста</param>
    /// <param name="cancellationToken">Распространяет уведомление о том, что операции следует отменить.</param>
    public Task<string> Handle(GetPostImageUrlQuery request, CancellationToken cancellationToken)
    {
        //Поиск организаций и возвращение коллекции 
        return imagesManager.GetAsync(request.ImageName);
    }
}
