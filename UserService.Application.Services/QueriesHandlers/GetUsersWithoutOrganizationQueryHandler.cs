using MediatR;
using UserService.Application.Abstractions.Queries;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.Aggregates.User;

namespace UserService.Application.Services.QueriesHandlers;

/// <summary>
/// Обработчик запроса для получения пользователей, которые не состоят не в одной организации
/// </summary>
/// <param name="userStore">Интефейс репозитория пользователей</param>
public class GetUsersWithoutOrganizationQueryHandler(IUserRepository userStore)
    : IRequestHandler<GetUsersWithoutOrganizationQuery, ICollection<UserAggregate>>
{
    public Task<ICollection<UserAggregate>> Handle(GetUsersWithoutOrganizationQuery request, CancellationToken cancellationToken)
    {
        //Проверка параметров запроса 
        if (request.Skip < 0 && request.Take <= 0)
        {
            //Если параметры неккоректные выбрасываем исключение
            throw new Exception("Неккоректные параметры запроса");
        }

        //Поиск пользователей без организации и возвращение коллекции
        return userStore.FindWithoutOrganizationAsync(request.Skip, request.Take);
    }
}