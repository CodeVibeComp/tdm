﻿using MediatR;
using Microsoft.Extensions.Logging;
using UserService.Application.Abstractions.Commands;
using UserService.Domain.Abstractions.Managers;
using UserService.Domain.Abstractions.UnitOfWork;

namespace UserService.Application.Services.CommandsHandlers;

/// <summary>
/// Обработчик команды обновления постов
/// </summary>
/// <param name="unitOfWork">Интерфейс единицы работы с репозиториями</param>
/// <param name="postImagesManager">Менеджер работы с изображениями</param>
/// <param name="logger">Логгер</param>
public class UpdatePostCommandHandler(IUnitOfWork unitOfWork,
    IPostImagesManager postImagesManager,
    ILogger<UpdatePostCommandHandler> logger) : IRequestHandler<UpdatePostCommand>
{
    /// <summary>
    /// Обработчик команды
    /// </summary>
    /// <param name="request">Команда для обновления поста</param>
    /// <param name="cancellationToken">Распространяет уведомление о том, что операции следует отменить.</param>
    public async Task Handle(UpdatePostCommand request, CancellationToken cancellationToken)
    {
        // Добавление изображения
        var imageName = await postImagesManager.AddAsync(request.ImageName, request.ImageContent);

        // Логгирование
        logger.LogInformation("Изображение {Name} было сохранено в хранилище", imageName);

        // Создание поста
        var post = await unitOfWork.PostRepository.GetAsync(request.Id);

        // Если пост был найден
        if (post != null)
        {
            post.ImageName = imageName;
            post.Description = request.Description;

            // Обновляем пост
            await unitOfWork.PostRepository.UpdateAsync(post);
        }
    }
}
