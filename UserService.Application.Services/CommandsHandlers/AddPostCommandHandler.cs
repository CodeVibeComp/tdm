﻿using MediatR;
using Microsoft.Extensions.Logging;
using UserService.Application.Abstractions.Commands;
using UserService.Domain.Abstractions.Managers;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.Abstractions.UnitOfWork;
using UserService.Domain.Aggregates.Post;

namespace UserService.Application.Services.CommandsHandlers;

/// <summary>
/// Обработчик команды добавления постов
/// </summary>
/// <param name="repository">Интерфейс репозитория постов</param>
/// <param name="postImagesManager">Менеджер работы с изображениями</param>
/// <param name="unitOfWork">Интерфейс единицы работы с репозиториями</param>
public class AddPostCommandHandler(IUnitOfWork unitOfWork,
    IPostImagesManager postImagesManager,
    ILogger<AddPostCommandHandler> logger) : IRequestHandler<AddPostCommand>
{
    /// <summary>
    /// Обработчик команды
    /// </summary>
    /// <param name="request">Команда для добавления поста</param>
    /// <param name="cancellationToken">Распространяет уведомление о том, что операции следует отменить.</param>
    public async Task Handle(AddPostCommand request, CancellationToken cancellationToken)
    {
        // Добавление изображения
        var imageName = await postImagesManager.AddAsync(request.ImageName, request.ImageContent);

        // Логгирование
        logger.LogInformation("Изображение {Name} было сохранено в хранилище", imageName);

        // Создание поста
        var post = PostAggregate.Create(
            Guid.NewGuid(),
            imageName,
            request.Description);

        // Добавляем новый пост в базу данных
        await unitOfWork.PostRepository.AddAsync(post);
    }
}
