using MediatR;
using Microsoft.Extensions.Logging;
using UserService.Domain.Abstractions.Repositories;
using UserService.Application.Abstractions.Commands;
using UserService.Application.Abstractions.Exceptions;
using UserService.Domain.Abstractions.Managers;
using UserService.Domain.Aggregates.User;

namespace UserService.Application.Services.CommandsHandlers;

/// <summary>
/// Обработчик запроса на связывание пользователя с организацией 
/// </summary>
/// <param name="userStore">Интерфейс репозитория управления пользователями</param>
/// <param name="cacheManager">Интерфейс управлением кеша</param>
/// <param name="organizationStore">Интерфейс репозитория управления организациями</param>
/// <param name="logger">Интерфейс логирования</param>
public class AddUserToOrganizationCommandHandler(
    IUserRepository userStore,
    ICacheManager cacheManager,
    IOrganizationRepository organizationStore,
    ILogger<AddUserToOrganizationCommandHandler> logger) : IRequestHandler<AddUserToOrganizationCommand>
{
    /// <summary>
    /// Обобработчик команды связывания пользователя с организацией 
    /// </summary>
    /// <param name="request">Класс запроса на связывание пользователя с организацией</param>
    /// <param name="cancellationToken">Токен для прерывания операции</param>
    /// <exception cref="UserNotFoundException">Исключение использующийся в случае отсутствию пользователя
    /// с искомым Id</exception>
    /// <exception cref="OrganizationNotFoundException">Исключение для ситуации когда не найдена организации
    /// по id</exception>
    public async Task Handle(AddUserToOrganizationCommand request, CancellationToken cancellationToken)
    {
        //Получение пользователя
        var user = await userStore.GetAsync(request.UserId);

        //Если пользователь не существует выбрасываем исключение
        if (user == null) throw new UserNotFoundException(request.UserId);

        //Получение организации
        var organization = await organizationStore.GetAsync(request.OrganizationId);

        //Если организация не существует выбрасываем исключение
        if (organization == null) throw new OrganizationNotFoundException(request.OrganizationId);

        // Получение пользователя из кеша
        var userFromCache = await cacheManager.GetAsync<UserAggregate>(user.Id.ToString());

        if (userFromCache == null) throw new CacheNotFoundException(user.Id.ToString());

        logger.LogInformation("Из кеша был получен пользователь {LastName} {FirstName} без организации",
            user.LastName,
            user.FirstName);

        // Удаление записи из кеша
        await cacheManager.DeleteAsync(user.Id.ToString());

        //Связывание пользователя с организацией
        user.SetToOrganization(organization);
        
        //Логирование успешного добавления пользователя в организацию
        logger.LogInformation("Пользователь {LastName} {FirstName} добавлен в организацию {OrgName}",
            user.LastName,
            user.FirstName, organization.Name);

        //Обновление пользователя
        await userStore.UpdateAsync(user);
    }
}