using MediatR;
using UserService.Domain.Abstractions.Repositories;
using UserService.Application.Abstractions.Commands;
using UserService.Domain.Abstractions.Managers;
using UserService.Domain.Aggregates.User;
namespace UserService.Application.Services.CommandsHandlers;

/// <summary>
/// Обработчик команды добавления пользователя 
/// </summary>
/// <param name="userStore"></param>
/// <param name="cacheManager"></param>
public class AddUserCommandHandler(IUserRepository userStore, ICacheManager cacheManager) : IRequestHandler<AddUserCommand>
{
    /// <summary>
    /// обработчик команды
    /// </summary>
    /// <param name="request">Команда для добавления пользователя</param>
    /// <param name="cancellationToken">Распространяет уведомление о том, что операции следует отменить.</param>
    /// <returns></returns>
    public Task Handle(AddUserCommand request, CancellationToken cancellationToken)
    {
        // Создание пользователя
        var user = UserAggregate.Create(
            Guid.NewGuid(),
            request.FirstName,
            request.LastName,
            request.Patronymic,
            request.Email,
            request.PhoneNumber);

        // Добавление пользователя в кеш
        cacheManager.AddAsync(user.Id.ToString(), user);

        // Добавление пользователя и отправка результата
        return userStore.AddAsync(user);
    }
}