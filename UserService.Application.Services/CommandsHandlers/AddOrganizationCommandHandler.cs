using MediatR;
using UserService.Application.Abstractions.Commands;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.Aggregates.Organization;

namespace UserService.Application.Services.CommandsHandlers;
/// <summary>
/// Обработчик команды добавления организации
/// </summary>
/// <param name="repository">Интерфейс репозитория организации</param>
public class AddOrganizationCommandHandler(IOrganizationRepository repository):IRequestHandler<AddOrganizationCommand>
{
    /// <summary>
    /// Обработчик команды
    /// </summary>
    /// <param name="request">Команда для добавления организации</param>
    /// <param name="cancellationToken">Распространяет уведомление о том, что операции следует отменить.</param>
    public Task Handle(AddOrganizationCommand request, CancellationToken cancellationToken)
    {
        // Создание организации
        OrganizationAggregate org = OrganizationAggregate.Create(Guid.NewGuid(), request.Name);
        
        // Добавление организации в бд
        return repository.AddAsync(org);
    }
}