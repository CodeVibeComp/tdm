﻿namespace Shared.Models;

/// <summary>
/// Сообщение о создании пользователя. ExchangeType Fanout 
/// </summary>
public class UserCreated
{
    /// <summary>
    /// Имя пользователя
    /// </summary>
    public required string FirstName { get; init;}

    /// <summary>
    /// Фамилия пользователя
    /// </summary>
    public required string LastName { get; init; }

    /// <summary>
    /// Отчество пользователя
    /// </summary>
    public string? Patronymic { get; init; }
    /// <summary>
    /// Номер телефона пользователя
    /// </summary>
    public required string PhoneNumber { get; init;}

    /// <summary>
    /// Адрес электронной почты пользователя
    /// </summary>
    public required string Email { get; init;}
}

    