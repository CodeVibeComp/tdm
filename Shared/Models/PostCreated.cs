﻿using Microsoft.AspNetCore.Http;

namespace Shared.Models;

/// <summary>
/// Сообщение о создании поста. ExchangeType Fanout 
/// </summary>
public class PostCreated
{
    /// <summary>
    /// Название изображения
    /// </summary>
    public required string ImageName { get; init; }

    /// <summary>
    /// Содержимое изображения
    /// </summary>
    public required byte[] ImageContent { get; init; }

    /// <summary>
    /// Описание поста
    /// </summary>
    public required string Description { get; init; }
}