using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Xml.Linq;
using UserService.Application.Abstractions.Queries;
using UserService.Presentation.Contracts.Organization;

namespace UserService.Presentation.Controllers;

/// <summary>
/// Контроллер управления организациями
/// </summary>
/// <param name="mediator">Интерфейс посредника</param>
[ApiController]
[Route("api/[controller]/[action]")]
public class OrganizationController(IMediator mediator) : ControllerBase
{
    /// <summary>
    /// Метод для получения данных организаций.
    /// </summary>
    /// <param name="page">Номер страницы</param>
    /// <param name="countPerPage">Количество записей на странице</param>
    /// <param name="token">Токен для отмены операции</param>
    [HttpGet]
    public async Task<ActionResult<OrganizationViewModel[]>> GetOrganizations(int page = 1, int countPerPage = 15,
        CancellationToken token = default)
    {
        //получаем количество записей для пропуска
        var skip = (page - 1) * countPerPage;
        
        //отправляем запрос для получения организаций
        var organizations = await mediator.Send(new GetOrganizationsQuery
        {
            Skip = skip,
            Take = countPerPage
        }, token);
        
        //получаем список ViewModel организаций
        var organizationsModels = organizations.Select(o => new OrganizationViewModel()
        {
            Id = o.Id,
            Name = o.Name,
        }).ToArray();
        
        //Возвращаем результат запроса
        return Ok(organizationsModels);
    }
}