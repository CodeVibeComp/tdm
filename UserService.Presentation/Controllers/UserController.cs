using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using UserService.Application.Abstractions.Commands;
using UserService.Application.Abstractions.Queries;
using UserService.Presentation.Contracts.User;

namespace UserService.Presentation.Controllers;

/// <summary>
/// Контроллер управления пользователями
/// </summary>
/// <param name="mediator">Интерфейс посредника</param>
/// <param name="validatorAddUserToOrganizationInputModel">Интефрейс валидации UpdateOrganizationInputModel</param>
[ApiController]
[Route("api/[controller]/[action]")]
public class UserController(
    IMediator mediator,
    IValidator<AddUserToOrganizationInputModel> validatorAddUserToOrganizationInputModel)
    : ControllerBase
{
    /// <summary>
    /// Метод для получения данных о пользователях, которые не состоят в ни одной организации
    /// </summary>
    /// <param name="page">Страница</param>
    /// <param name="countPerPage">Количество записей на странице</param>
    /// <param name="token">Токен отмены</param>
    [HttpGet]
    public async Task<ActionResult<UserViewModel[]>> GetUsersWithoutOrganization(int page = 1, int countPerPage = 15,
        CancellationToken token = default)
    {
        // Получаем количество пропущенных записей
        var skip = (page - 1) * countPerPage;

        // Отправляем запрос на получение пользователей без организации
        var users = await mediator.Send(new GetUsersWithoutOrganizationQuery
        {
            Skip = skip,
            Take = countPerPage
        }, token);

        // Получаем записи пользователей в виде UserViewModel
        var usersResponse = users.Select(u => new UserViewModel
        {
            Id = u.Id,
            FirstName = u.FirstName,
            LastName = u.LastName,
            Patronymic = u.Patronymic,
            PhoneNumber = u.PhoneNumber,
            Email = u.Email,
        }).ToArray();

        // Отдаем результат запроса
        return Ok(usersResponse);
    }

    /// <summary>
    /// Метод для получения данных о пользователях, указанной организации
    /// </summary>
    /// <param name="organizationId">Идентификатор организации</param>
    /// <param name="page">Страница</param>
    /// <param name="countPerPage">Количество записей на странице</param>
    /// <param name="token">Токен отмены</param>
    /// <response code="200">Запрос выполнен успешно</response>
    [HttpGet]
    public async Task<ActionResult<UserViewModel[]>> GetUsers(Guid? organizationId, int page = 1, int countPerPage = 15,
        CancellationToken token = default)
    {
        // Получаем число пропущенных записей
        var skip = (page - 1) * countPerPage;

        // Отправляем запрос на получение пользователей у указанной организации
        var users = await mediator.Send(new GetUsersQuery
        {
            OrganizationId = organizationId,
            Skip = skip,
            Take = countPerPage
        }, token);

        // Получаем записи пользователей в виде UserViewModel
        var usersResponse = users.Select(u => new UserViewModel
        {
            Id = u.Id,
            FirstName = u.FirstName,
            LastName = u.LastName,
            Patronymic = u.Patronymic,
            PhoneNumber = u.PhoneNumber,
            Email = u.Email,
        }).ToArray();

        // Отдаем результат запроса
        return Ok(usersResponse);
    }

    ///<summary>
    ///Метод позволяет связать пользователя с организацией 
    ///</summary>
    ///<param name="model">Модель запроса на связывание пользователя с организацией</param>
    ///<param name="token">Токен для отмены операции</param>
    ///<response code="200">Запрос выполнен успешно</response>
    ///<response code="400">Запрос не был выполнен, т.к. данные не валидны</response>
    [HttpPut]
    public async Task<ActionResult> AddToOrganization(AddUserToOrganizationInputModel model, CancellationToken token)
    {
        // Получение резултата валидации данных
        var validationResult = await validatorAddUserToOrganizationInputModel.ValidateAsync(model, token);

        // Проверка резултата
        if (!validationResult.IsValid)
        {
            // Если данные не валидны, отправляем BadRequest
            return BadRequest(new
            {
                Errors = validationResult.Errors.Select(x => $"{x.PropertyName}: {x.ErrorMessage}").ToArray()
            });
        }

        // Отправляем запрос на связывание пользователя с организацией
        await mediator.Send(new AddUserToOrganizationCommand
        {
            UserId = model.Userid!.Value,
            OrganizationId = model.OrganizationId!.Value
        }, token);

        // Возвращаем 200
        return Ok();
    }
}