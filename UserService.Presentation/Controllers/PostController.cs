﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using UserService.Application.Abstractions.Commands;
using UserService.Application.Abstractions.Queries;
using UserService.Presentation.Contracts.Post;
namespace UserService.Presentation.Controllers;

/// <summary>
/// Контроллер управления постами
/// </summary>
/// <param name="mediator">Интерфейс посредника</param>
[ApiController]
[Route("api/[controller]/[action]")]
public class PostController(IMediator mediator,
    IValidator<UpdatePostInputModel> updatePostInputModelValidator) : ControllerBase
{
    /// <summary>
    /// Метод для получения данных постов.
    /// </summary>
    /// <param name="page">Номер страницы</param>
    /// <param name="countPerPage">Количество записей на странице</param>
    /// <param name="token">Токен для отмены операции</param>
    [HttpGet]
    public async Task<ActionResult<PostViewModel[]>> GetPosts(int page = 1, int countPerPage = 15,
        CancellationToken token = default)
    {
        // Получаем количество записей для пропуска
        var skip = (page - 1) * countPerPage;

        // Отправляем запрос для получения постов
        var posts = await mediator.Send(new GetPostsQuery
        {
            Skip = skip,
            Take = countPerPage
        }, token);

        // Получаем список ViewModel постов
        var postsModels = await Task.WhenAll(posts.Select(async p => new PostViewModel
        {
            Id = p.Id,
            ImageUrl = await mediator.Send(new GetPostImageUrlQuery { ImageName = p.ImageName }, token),
            Description = p.Description,
        }));

        // Возвращаем результат запроса
        return Ok(postsModels);
    }

    ///<summary>
    ///Метод позволяет обновить пост
    ///</summary>
    ///<param name="model">Модель запроса на обновление поста</param>
    ///<param name="token">Токен для отмены операции</param>
    ///<response code="200">Запрос выполнен успешно</response>
    ///<response code="400">Запрос не был выполнен, т.к. данные не валидны</response>
    [HttpPut]
    public async Task<ActionResult> UpdatePost([FromForm] UpdatePostInputModel model, CancellationToken token)
    {
        // Получение резултата валидации данных
        var validationResult = await updatePostInputModelValidator.ValidateAsync(model, token);

        // Проверка резултата
        if (!validationResult.IsValid)
        {
            // Если данные не валидны, отправляем BadRequest
            return BadRequest(new
            {
                Errors = validationResult.Errors.Select(x => $"{x.PropertyName}: {x.ErrorMessage}").ToArray()
            });
        }

        // Создаем новый MemoryStream
        await using var imageFileStream = new MemoryStream();

        // Копируем содержимое изображения в MemoryStream
        await model.Image.CopyToAsync(imageFileStream);

        // Отправляем команду на добавление поста
        await mediator.Send(
            new UpdatePostCommand
            {
                Id = model.Id!.Value,
                ImageName = model.Image.FileName,
                ImageContent = imageFileStream.ToArray(),
                Description = model.Description,
            }, token);

        // Возвращаем 200
        return Ok();
    }
}