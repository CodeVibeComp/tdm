﻿namespace UserService.Presentation.Contracts.Post;

/// <summary>
/// Выводная модель поста для контроллера
/// </summary>
/// <param name="Id">Идентификатор поста</param>
/// <param name="ImageName">Название изображения в хранилище</param>
/// <param name="Description">Описание поста</param>
public class PostViewModel
{
    // Идентификатор поста
    public Guid Id { get; set; }

    // Ссылка на изображение
    public string ImageUrl { get; set; }

    // Описание
    public string Description { get; set; }
}