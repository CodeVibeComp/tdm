﻿using Microsoft.AspNetCore.Http;

namespace UserService.Presentation.Contracts.Post;

/// <summary>
/// Входная модель обновления поста
/// </summary>
/// <param name="Userid">Идентификатор пользователя</param>
/// <param name="OrganizationId">Идентификатор организации</param>
public class UpdatePostInputModel
{
    // Идентификатор поста
    public Guid? Id { get; set; }

    // Файл изображения
    public IFormFile? Image { get; set; }

    // Описание
    public string? Description { get; set; }
}
