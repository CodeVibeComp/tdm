﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using UserService.Domain.Aggregates.Post;

namespace UserService.Presentation.Contracts.Post.Validators;

/// <summary>
/// Класс предназначен для валидации объектов типа UpdatePostInputModel.
/// Он содержит правила которые определяют, какие условия должны быть выполнены для объекта
/// </summary>
public class UpdatePostInputModelValidator : AbstractValidator<UpdatePostInputModel>
{
    // Максимальный размер изображения
    public int MaxFileSize { get; set; }

    // Разрешенные форматы файла
    public string[] ImageTypes { get; set; }

    /// <summary>
    /// Правила валидации
    /// </summary>
    public UpdatePostInputModelValidator(IConfiguration configuration)
    {
        // Получаем из конфига максимальный размер файла в байтах
        MaxFileSize = int.Parse(configuration["ValidationParams:MaxFileSize"]);

        // Получаем из конфига разрешенные форматы файла
        ImageTypes = configuration.GetSection("ValidationParams:ImageTypes").GetChildren().Select(c => c.Value).ToArray();

        // Получаем из конфига минимальную длинну описания поста
        var minDescLength = SettingsPostAggregate.MinDescLength;

        // Получаем из конфига максимальную длинну описания поста
        var maxDescLength = SettingsPostAggregate.MaxDescLength;

        // Устанавливаем правило валидации описания
        RuleFor(e => e.Description)
            .NotEmpty().WithMessage("Описание поста является обязательным полем.")
            .MinimumLength(minDescLength).WithMessage("Описание поста должно составлять как минимум 5 символов.")
            .MaximumLength(maxDescLength).WithMessage("Описание поста не может превышать 350 символов.");

        // Устанавливаем правило валидации изображения
        RuleFor(e => e.Image)
            .Must(BeValidImage).WithMessage($"Изображение должно быть обязательным полем формата {string.Join(", ", ImageTypes)}");
    }

    /// <summary>
    /// Метод проверки формата файла на сооветствие изображению
    /// </summary>
    /// <param name="model">Модель</param>
    /// <param name="image">Файл IFormFile</param>
    /// <returns>Task</returns>
    private bool BeValidImage(UpdatePostInputModel model, IFormFile image)
    {
        // Проверка на пустое изображение 
        if (image == null)
            return false;

        // Проверка на превышение размера файла
        if (image.Length > MaxFileSize)
            return false;

        // Приводим путь к изображению в нижний регистр
        var extension = Path.GetExtension(image.FileName).ToLower();

        // Проверка на соответствие разрешенному формату файла
        if (!ImageTypes.Contains(extension))
            return false;

        // Если проверка пройдена успешно валидируем
        return true;
    }
}
