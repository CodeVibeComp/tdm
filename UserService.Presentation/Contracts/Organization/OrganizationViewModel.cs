namespace UserService.Presentation.Contracts.Organization;

/// <summary>
/// Выводная модель организации для контроллера
/// </summary>
/// <param name="Id">Идентификатор организации</param>
/// <param name="Name">Название организации</param>
public class OrganizationViewModel
{
    // Идентификатор организации
    public Guid Id { get; set; }

    // Название организации
    public string Name { get; set; }
}