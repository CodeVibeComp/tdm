using FluentValidation;

namespace UserService.Presentation.Contracts.User.Validators;

/// <summary>
/// Класс предназначен для валидации объектов типа AddOrganizationInputModel.
/// Он содержит правила (rules), которые определяют, какие условия должны быть выполнены для объекта
/// </summary>
public class AddUserToOrganizationInputModelValidator: AbstractValidator<AddUserToOrganizationInputModel>
{
    /// <summary>
    /// Настройка правил валидации
    /// </summary>
    public AddUserToOrganizationInputModelValidator() 
    {
        // Правила проверки идентификатора пользователя
        RuleFor(x => x.Userid)
            .NotNull().WithMessage("UserId не может быть пустым.")
            .NotEmpty().WithMessage("UserId не может быть пустым.");

        // Правила проверки идентификатора организации
        RuleFor(x => x.OrganizationId)
            .NotNull().WithMessage("OrganizationId не может быть пустым.")
            .NotEmpty().WithMessage("OrganizationId не может быть пустым.");
    }
}