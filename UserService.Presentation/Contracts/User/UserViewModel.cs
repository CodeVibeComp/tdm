namespace UserService.Presentation.Contracts.User;

/// <summary>
/// Выводная модель пользователя для контроллера
/// </summary>
/// <param name="Id">Идентификатор пользователя</param>
/// <param name="FirstName">Имя</param>
/// <param name="LastName">Фамилия</param>
/// <param name="Patronymic">Отчество</param>
/// <param name="PhoneNumber">Номер телефона</param>
/// <param name="Email">Электронная почта</param>
public class UserViewModel
{
    // Идентификатор пользователя
    public Guid Id { get; set; }

    // Имя пользователя
    public string FirstName { get; set; }

    // Фамилия пользователя 
    public string LastName { get; set; }

    // Отчество
    public string? Patronymic { get; set; }

    // Номер телефона
    public string PhoneNumber { get; set; }

    // Адрес электронной почты
    public string Email { get; set; }
}