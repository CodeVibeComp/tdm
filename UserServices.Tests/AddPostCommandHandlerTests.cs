﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using UserService.App.Settings;
using UserService.Application.Abstractions.Commands;
using UserService.Application.Services.CommandsHandlers;
using UserService.Domain.Abstractions.Managers;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.Abstractions.UnitOfWork;
using UserService.Domain.Aggregates.Post;

namespace UserServices.Tests;

/// <summary>
/// Тест работоспособности обработчика добавления новых постов
/// </summary>
public class AddPostCommandHandlerTests
{
    // Мок интерфейса IUnitOfWork
    private Mock<IUnitOfWork> _unitOfWorkMock = null!;

    // Мок интерфейса IPostRepository
    private Mock<IPostRepository> _postRepositoryMock = null!;

    // Мок интерфейса IPostImagesManager
    private Mock<IPostImagesManager> _imagesManagerMock = null!;

    // Мок интерфейса  <ILogger<AddPostCommandHandler>>
    private Mock<ILogger<AddPostCommandHandler>> _logger = null!;

    // Экземпляра класса AddPostCommandHandler
    private AddPostCommandHandler _postCommandHandler = null!;

    // Создание объекта конфигуратора
    private IConfiguration _configuration;

    // Создание коллекции сервисов
    private IServiceCollection _services;

    /// <summary>
    /// Конструктор, вызываемый перед каждым тестом
    /// </summary>
    [SetUp]
    public void SetUp()
    {
        // Создание экземпляра IConfiguration
        _configuration = new ConfigurationBuilder()
            .AddJsonFile("Configuration/appsettings.json")
            .Build();

        // Создание экземпляра IServiceCollection
        _services = new ServiceCollection();

        // Добавление настроек агрегатов в коллекцию сервисов
        _services.AddAggregatesSettings(_configuration);

        // Создание мока для интерфейса IUnitOfWork
        _unitOfWorkMock = new Mock<IUnitOfWork>();

        // Создание мока для интерфейса IPostRepository
        _postRepositoryMock = new Mock<IPostRepository>();

        // Создание мока для интерфейса IPostImagesManager
        _imagesManagerMock = new Mock<IPostImagesManager>();

        // Создание мока для интерфейса ILogger
        _logger = new Mock<ILogger<AddPostCommandHandler>>();

        // Создание экземпляра класса AddPostCommandHandler с переданными моками
        _postCommandHandler = new AddPostCommandHandler(_unitOfWorkMock.Object, _imagesManagerMock.Object, _logger.Object);
    }

    /// <summary>
    /// Тест на валидное добавление постов
    /// </summary>
    /// <param name="imageName">Название файла изображения</param>
    /// <param name="content">Содержимое файла в формате byte[]</param>
    /// <param name="description">Описание поста</param>
    [TestCase("cat.png", new byte[] { 1 }, "Мой первый пост")]
    [TestCase("dog.jpg", new byte[] { 1 }, "Мой второй пост")]
    [TestCase("dog.jpeg", new byte[] { 1 }, "Мой последний пост")]
    public async Task Handle_ValidPost_AddsPostToStore(string imageName, byte[] content, string description)
    {
        // Создаем новую команду добавления поста
        var command = new AddPostCommand
        {
            ImageName = imageName,
            ImageContent = content,
            Description = description
        };

        // Настраиваем поведение мока на возвращение названия файла при его добавлении
        _imagesManagerMock.Setup(x => x.AddAsync(imageName, content)).ReturnsAsync(Guid.NewGuid().ToString());

        // Настраиваем поведение мока на возвращение мока репозитория постов
        _unitOfWorkMock.SetupGet(u => u.PostRepository).Returns(_postRepositoryMock.Object);

        // Вызываем комманду добавления поста
        await _postCommandHandler.Handle(command, CancellationToken.None);

        // Убеждаемся, что было добавление поста в базу данных
        _unitOfWorkMock.Verify(p => p.PostRepository.AddAsync(It.IsAny<PostAggregate>()), Times.Once);
    }
}
