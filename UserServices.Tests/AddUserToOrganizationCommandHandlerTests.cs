using Microsoft.Extensions.Logging;
using Moq;
using UserService.Application.Abstractions.Commands;
using UserService.Application.Abstractions.Exceptions;
using UserService.Application.Services.CommandsHandlers;
using UserService.Domain.Abstractions.Managers;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.Aggregates.Organization;
using UserService.Domain.Aggregates.User;
using UserService.Domain.Aggregates.User.Exceptions;

namespace UserServices.Tests;

/// <summary>
/// Тест работоспособности обработчика добавления пользователя в организацию
/// </summary>
[TestFixture]
public class AddUserToOrganizationCommandHandlerTests
{
    // Объект пользователя
    private UserAggregate _user = null!;

    // Объект организации
    private OrganizationAggregate _organization = null!;

    // Мок интерфейса логгера
    private Mock<ILogger<AddUserToOrganizationCommandHandler>> _loggerMock = null!;

    // Мок интерфейса менеджера кеша
    private Mock<ICacheManager> _cacheManagerMock = null!;

    /// <summary>
    /// Конструктор, вызываемый перед каждым тестом
    /// </summary>
    [SetUp]
    public void SetUp()
    {
        // Инициализация интерфейса мока логгера
        _loggerMock = new Mock<ILogger<AddUserToOrganizationCommandHandler>>();

        // Создание пользователя
        _user = UserAggregate.Create(
            id: Guid.NewGuid(), 
            firstName: "Максим", 
            lastName: "Завгородний", 
            patronymic: "Валерьевич",
            phoneNumber: "+79396743685", 
            email: "abc@yandex.rom");

        // Создание организации
        _organization = OrganizationAggregate.Create(Guid.NewGuid(), "Тестовая организация");
    }

    /// <summary>
    /// Валидный тест обработчика на добавления пользователя в организацию
    /// </summary>
    [Test]
    public async Task Handle_ValidCommand_AddsUserToOrganization()
    {
        // Инициализируем мок репозитория пользователей
        var userStoreMock = new Mock<IUserRepository>();

        // Настройка, что мок при добавлении пользователя возвращает его
        userStoreMock.Setup(x => x.GetAsync(_user.Id)).ReturnsAsync(_user);

        // Инициализируем мок репозитория организаций
        var organizationStoreMock = new Mock<IOrganizationRepository>();

        // Настройка, что мок при добавлении организации вернёт её
        organizationStoreMock.Setup(x => x.GetAsync(_organization.Id)).ReturnsAsync(_organization);

        // Инициализируем мок кеш менеджера
        _cacheManagerMock = new Mock<ICacheManager>();

        // Настройка, что кеш менеджера вернет пользователя при его вызове
        _cacheManagerMock.Setup(x => x.GetAsync<UserAggregate>(_user.Id.ToString())).ReturnsAsync(_user);

        // Создание команды добавления пользователя в организацию
        var request = new AddUserToOrganizationCommand
        {
            OrganizationId = _organization.Id,
            UserId = _user.Id
        };
        
        // Создание обработчка команды добавления пользователя в организацию
        var handler =
            new AddUserToOrganizationCommandHandler(userStoreMock.Object, _cacheManagerMock.Object, organizationStoreMock.Object,
                _loggerMock.Object);

        // Вызов обработчика
        await handler.Handle(request, CancellationToken.None);

        // Ожидаем, что пользователь был обновлен в базе данных
        userStoreMock.Verify(x => x.UpdateAsync(_user), Times.Once);

        // Ожидаем, что логгер отправил лог о добавлении пользователя в организацию
        _loggerMock.VerifyLog(logger => logger.LogInformation(
            "Пользователь {LastName} {FirstName} добавлен в организацию {OrgName}", _user.LastName,
            _user.FirstName, _organization.Name));
    }

    /// <summary>
    /// Тест на обработку ошибки во добавления пользователя в организацию, когда он уже состоит в ней
    /// </summary>
    [Test]
    public Task Handle_WhenUserAlreadyInOrganization_ThrowsUserAlreadyInOrganizationException()
    {
        // Создаем пользователя
        var user = UserAggregate.Create(
            id: Guid.NewGuid(), 
            firstName: "Максим", 
            lastName: "Завгородний", 
            patronymic: "Валерьевич",
            phoneNumber: "+79396743685", 
            email: "abc@yandex.rom");
        
        // Создаем организацию
        var userOrganization = OrganizationAggregate.Create(Guid.NewGuid(),"Организация пользователя");

        // Напрямую добавляем пользователя в организацию
        user.SetToOrganization(userOrganization);

        // Создаем команду добавления пользователя в организацию
        var request = new AddUserToOrganizationCommand
        {
            OrganizationId = _organization.Id,
            UserId = user.Id
        };

        // Инициализация мока репозитория пользователей
        var userStoreMock = new Mock<IUserRepository>();

        // Настройка, что при получении пользователя из базы, мок  его вернет
        userStoreMock.Setup(x => x.GetAsync(request.UserId)).ReturnsAsync(user);

        // Инициализация мока репозитория организаций
        var organizationStoreMock = new Mock<IOrganizationRepository>();

        // Настройка, что при получении организации из базы, мок её вернет
        organizationStoreMock.Setup(x => x.GetAsync(request.OrganizationId)).ReturnsAsync(_organization);

        // Настройка, что при получении организации пользователя из базы, мок её вернет
        organizationStoreMock.Setup(x => x.GetAsync(user.OrganizationId!.Value)).ReturnsAsync(userOrganization);

        // Инициализация мока менеджера кеша
        _cacheManagerMock = new Mock<ICacheManager>();

        // Настройка, что кеш при запросе возвращает пользователя
        _cacheManagerMock.Setup(x => x.GetAsync<UserAggregate>(user.Id.ToString())).ReturnsAsync(user);

        // Создание обработчика добавления пользователя в организацию
        var handler =
            new AddUserToOrganizationCommandHandler(userStoreMock.Object, _cacheManagerMock.Object,
            organizationStoreMock.Object, _loggerMock.Object);

        // Ожидаем, что ошибка была вызвана после запуска обработчика
        var exception =
            Assert.ThrowsAsync<UserAlreadyInOrganizationException>(() =>
                handler.Handle(request, CancellationToken.None));

        // Ожидаем, что ошибка не пустая
        Assert.That(exception, Is.Not.Null);

        // Ожидаем, что пользователь в ошибке совпадает с отправленным в обработчик пользователем
        Assert.That(exception!.IdUser, Is.EqualTo(request.UserId));

        // Возвращаем успешное завершение теста
        return Task.CompletedTask;
    }

    /// <summary>
    /// Тест на обработку ошибки во добавления пользователя в организацию,
    /// когда она не была найдена
    /// </summary>
    [Test]
    public Task Handle_WhenOrganizationNotFound_ThrowsOrganizationNotFoundException()
    {
        // Генерируем id организации
        var orgId = Guid.NewGuid();

        // Создаем комманду добавления пользователя в организацию
        var request = new AddUserToOrganizationCommand
        {
            OrganizationId = orgId,
            UserId = _user.Id
        };

        // Инициализируем мок интерфейса репозитория пользователей
        var userStore = new Mock<IUserRepository>();

        // Инициализируем мок интерфейса репозитория организаций
        var organizationStore = new Mock<IOrganizationRepository>();

        // Настраиваем мок, что при запросе пользователя - он вернется
        userStore.Setup(x => x.GetAsync(request.UserId)).ReturnsAsync(_user);

        // Настраиваем мок, что при запросе организации - она вернется
        organizationStore.Setup(x => x.GetAsync(request.OrganizationId)).ReturnsAsync(() => null);

        // Инициализируем мок менеджера кеша
        _cacheManagerMock = new Mock<ICacheManager>();

        // Создаем обработчик команды добавления пользователя в организацию
        var handler =
            new AddUserToOrganizationCommandHandler(userStore.Object, _cacheManagerMock.Object, organizationStore.Object, _loggerMock.Object);

        // Ожидаем, что ошибка была вызвана после запуска обработчика
        var exception = Assert.ThrowsAsync<OrganizationNotFoundException>(() =>
            handler.Handle(request, CancellationToken.None));

        // Ожидаем, что ошибка не пустая
        Assert.That(exception, Is.Not.Null);

        // Ожидаем, что id ошибки совпадает с id огранизации
        Assert.That(exception!.Id, Is.EqualTo(orgId));

        // Ожидаем, что при вызове обновления пользователя туда был передан пользователь
        userStore.Verify(x => x.UpdateAsync(It.IsAny<UserAggregate>()), Times.Never);

        // Возвращаем успешное завершение теста
        return Task.CompletedTask;
    }

    /// <summary>
    /// Тест на обработку ошибки во добавления пользователя в организацию,
    /// когда пользователь не был найден
    /// </summary>
    [Test]
    public Task Handle_WhenUserNotFound_ThrowsUserNotFoundException()
    {
        // Генерируем новый id пользователя
        var userId = Guid.NewGuid();

        // Создаем команду добавления пользователя в организацию
        var request = new AddUserToOrganizationCommand
        {
            OrganizationId = _organization.Id,
            UserId = userId
        };

        // Инициализация мока интерфейса репозитория пользователей
        var userStore = new Mock<IUserRepository>();

        // Инициализация мока интерфейса репозитория организаций
        var organizationStore = new Mock<IOrganizationRepository>();

        // Настройка мока, что при вызове пользователя будет возвращена пустота
        userStore.Setup(x => x.GetAsync(request.UserId)).ReturnsAsync(() => null);

        // Настраиваем мок, что при запросе организации - она вернется
        organizationStore.Setup(x => x.GetAsync(request.OrganizationId)).ReturnsAsync(_organization);

        // Инициализация мока интерфейса менеджера кеша
        _cacheManagerMock = new Mock<ICacheManager>();

        // Создаем обработчик добавления пользователя в организацию
        var handler =
            new AddUserToOrganizationCommandHandler(userStore.Object, _cacheManagerMock.Object, organizationStore.Object, _loggerMock.Object);

        // Ожидаем, что ошибка была вызвана после запуска обработчика
        var exception = Assert.ThrowsAsync<UserNotFoundException>(() =>
            handler.Handle(request, CancellationToken.None));

        // Ожидаем, что ошибка не пустая
        Assert.That(exception, Is.Not.Null);

        // Ожидаем, что id ошибки совпадает с id клиента
        Assert.That(exception!.Id, Is.EqualTo(request.UserId));

        // Ожидаем, что при вызове обновления пользователя туда был передан пользователь
        userStore.Verify(x => x.UpdateAsync(It.IsAny<UserAggregate>()), Times.Never);

        // Возвращаем успешное завершение теста
        return Task.CompletedTask;
    }
}