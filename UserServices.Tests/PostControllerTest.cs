﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using UserService.Presentation.Contracts.Post.Validators;
using UserService.Presentation.Controllers;
using MediatR;
using UserService.Application.Abstractions.Queries;
using UserService.Presentation.Contracts.Post;
using UserService.Application.Abstractions.Commands;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UserService.App.Settings;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;
using UserService.Domain.Aggregates.PostAggregate;
using UserService.Domain.Aggregates.Post;

namespace UserServices.Tests;

/// <summary>
/// Тест работоспособности контроллера постов
/// </summary>
[TestFixture]
public class PostControllerTest
{
    // Контроллер постов
    private PostController _controller = null!;

    // Мок интерфейса IMediator
    private Mock<IMediator> _mediatorMock = null!;

    // Создание объекта конфигуратора
    private IConfiguration configuration;

    // Создание коллекции сервисов
    private IServiceCollection services;

    /// <summary>
    /// Конструктор, вызываемый перед каждым тестом
    /// </summary>
    [SetUp]
    public void SetUp()
    {
        // Создание экземпляра IConfiguration
        configuration = new ConfigurationBuilder()
            .AddJsonFile("Configuration/appsettings.json")
            .Build();

        // Создание экземпляра IServiceCollection
        services = new ServiceCollection();

        // Добавление настроек агрегатов в коллекцию сервисов
        services.AddAggregatesSettings(configuration);

        // Создание мока для интерфейса IPostRepository
        _mediatorMock = new Mock<IMediator>();

        // Создание объекта валидатора обновления поста
        var validator = new UpdatePostInputModelValidator(configuration);

        // Передаем параметры в контроллер
        _controller = new PostController(_mediatorMock.Object, validator);
    }

    /// <summary>
    /// Валидный тест на запрос получения постов
    /// </summary>
    [Test]
    public async Task GetPosts_ReturnsPosts()
    {
        // Создание списка постов
        var posts = new List<PostAggregate>
            {
                PostAggregate.Create(Guid.NewGuid(),
                    "image1",
                    "Пост с котенком"),

                 PostAggregate.Create(Guid.NewGuid(),
                    "image2",
                    "Я на море"),

                 PostAggregate.Create(Guid.NewGuid(),
                    "image3",
                    "Мой последний пост"),
            };

        // При вызове GetPostsQuery должен возвращать наши посты
        _mediatorMock.Setup(x => x.Send(It.IsAny<GetPostsQuery>(), CancellationToken.None))
            .ReturnsAsync(posts);

        // Получаем посты из контроллера
        var result = (await _controller.GetPosts(1, 15, CancellationToken.None));

        // Приводим ObjectResult к ViewModel постов 
        var resultPosts = (PostViewModel[])((ObjectResult)result.Result).Value;

        // Проверяем тесты
        Assert.Multiple(() =>
        {
            // Результат не должен быть пустым
            Assert.That(result, Is.Not.Null);

            // Результат не должен быть пустым
            Assert.That(resultPosts, Is.Not.Null);

            // Число записанных и полученных постов должно совпадать
            Assert.That(resultPosts.Length, Is.EqualTo(posts.Count));
        });
    }

    /// <summary>
    /// Валидный тест на запрос обновление поста
    /// </summary>
    [Test]
    public async Task UpdatePost_WithValidModel_ReturnsOk()
    {
        // Создали заглушку содержимого обновляемого файла
        var stream = new MemoryStream([1]);

        // Создали загружаемую в контроллер модель поста
        var model = new UpdatePostInputModel
        {
            Id = Guid.NewGuid(),
            Image = new FormFile(stream, 0, 1, "image", "image.png"),
            Description = "Отредактировал пост",
        };

        // Мок на то, что UpdatePostCommand возвращает успех
        _mediatorMock.Setup(x => x.Send(It.IsAny<UpdatePostCommand>(), CancellationToken.None))
            .Returns(Task.CompletedTask);

        // Передали модель в контроллер
        var result = await _controller.UpdatePost(model, CancellationToken.None) as OkResult;

        // Проверяем, что результат не пустой
        Assert.That(result, Is.Not.Null);

        // Проверяем, что контроллер вернул успешный (200) ответ
        Assert.That(result!.StatusCode, Is.EqualTo(200));
    }
}
