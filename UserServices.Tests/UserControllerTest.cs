using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using UserService.Application.Abstractions.Commands;
using UserService.Application.Abstractions.Queries;
using UserService.Domain.Aggregates.User;
using UserService.Presentation.Contracts.User;
using UserService.Presentation.Contracts.User.Validators;
using UserService.Presentation.Controllers;

namespace UserServices.Tests;

/// <summary>
/// Тест работоспособности контроллера пользователей
/// </summary>
[TestFixture]
public class UserControllerTests
{
    // Контроллер пользователей
    private UserController _controller = null!;

    // Мок интерфейса IMediator
    private Mock<IMediator> _mediatorMock = null!;

    /// <summary>
    /// Валидный тест на запрос получения постов
    /// </summary>
    [SetUp]
    public void SetUp()
    {
        // Реализация мока интерфейса IMediator
        _mediatorMock = new Mock<IMediator>();

        // Реализация валидатора добавления клиента в организацию
        var validator = new AddUserToOrganizationInputModelValidator();

        // Реализация контроллера с параметрами
        _controller = new UserController(_mediatorMock.Object, validator);
    }

    /// <summary>
    /// Валидный тест на запрос получения постов
    /// </summary>
    [Test]
    public async Task GetUsers_ReturnsUsers()
    {
        // Генерируем id организации
        var organizationId = Guid.NewGuid();

        // Создаем пользователей
        var users = new List<UserAggregate>
        {
            UserAggregate.Create(Guid.NewGuid(),
                "Максим",
                "Завгородний",
                "Валерьевич",
                "zvg@yandex.com",
            "+79586745866"),

            UserAggregate.Create(Guid.NewGuid(),
                "Николай",
                "Загородный",
                null,
                "zfgfgg@yandex.ru",
                "+79345645856"),

        };

        // Настраиваем мок, что при запросе клиентов, будут возвращаны созданные нами ранее
        _mediatorMock.Setup(x => x.Send(It.IsAny<GetUsersQuery>(), CancellationToken.None))
            .ReturnsAsync(users);

        // Вызываем в контроллере список пользователей
        var result = await _controller.GetUsers(organizationId, 1, 15, CancellationToken.None);

        // Приводим ObjectResult к нужному нам UserViewModel[]
        var resultUsers = (UserViewModel[])((ObjectResult)result.Result).Value;

        // Вызываем тесты
        Assert.Multiple(() =>
        {
            // Ожидаем, что результат не пустой
            Assert.That(result, Is.Not.Null);

            // Ожидаем, что список пользователей не пустой
            Assert.That(resultUsers, Is.Not.Null);

            // Ожидаем, что число клиентов равное созданому нами ранее
            Assert.That(resultUsers.Length, Is.EqualTo(users.Count));
        });
    }

    /// <summary>
    /// Валидный тест контроллера на добавление пользователей в организацию
    /// </summary>
    [Test]
    public async Task AddToOrganization_WithValidModel_ReturnsOk()
    {
        // Создаем модель контроллера для добавления пользователя в организацию
        var model = new AddUserToOrganizationInputModel
        {
            OrganizationId = Guid.NewGuid(),
            Userid = Guid.NewGuid(),
        };

        // Настраиваем мок, чтобы при вызове добавления пользователя в организацию возвращался успешный результат
        _mediatorMock.Setup(x => x.Send(It.IsAny<AddUserToOrganizationCommand>(), CancellationToken.None))
            .Returns(Task.CompletedTask);

        // Получаем результат работы добавления клиента в организацию в контроллере
        var result = await _controller.AddToOrganization(model, CancellationToken.None) as OkResult;

        // Ожидаем, что результат не пустой
        Assert.That(result, Is.Not.Null);

        // Ожидаем, что ответ результата 200 (успешный)
        Assert.That(result!.StatusCode, Is.EqualTo(200));
    }

    /// <summary>
    /// Инвлидный тест контроллера на добавление пользователей в организацию с инвалидными параметрами
    /// </summary>
    [Test]
    public async Task AddToOrganization_WithInvalidModel_ReturnsBadRequest()
    {
        // Создаем инвалидную модель добавления пользователя в организацию
        var model = new AddUserToOrganizationInputModel();

        // Получаем результат добавления в контроллере
        var result = await _controller.AddToOrganization(model, CancellationToken.None) as BadRequestObjectResult;

        // Ожидаем, что ответ не пустой
        Assert.That(result, Is.Not.Null);

        // Ожидаем, что код ответа 400
        Assert.That(result!.StatusCode, Is.EqualTo(400));
    }
}