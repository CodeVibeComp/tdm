using Moq;
using UserService.Application.Abstractions.Commands;
using UserService.Application.Services.CommandsHandlers;
using UserService.Domain.Abstractions.Managers;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.Aggregates.User;

namespace UserServices.Tests;

/// <summary>
/// Тест работоспособности обработчика добавления новых пользователей
/// </summary>
public class AddUserCommandHandlerTests
{
    // Мок интерфейса IUserRepository
    private Mock<IUserRepository> _userStoreMock = null!;

    // Мок интерфейса ICacheManager
    private Mock<ICacheManager> _cacheManagerMock = null!;

    // Объект обработчика команды добавления пользователя
    private AddUserCommandHandler _userCommandHandler = null!;

    /// <summary>
    /// Конструктор, вызываемый перед каждым тестом
    /// </summary>
    [SetUp]
    public void SetUp()
    {
        // Создание мока для интерфейса репозитория пользователей
        _userStoreMock = new Mock<IUserRepository>();

        // Создание мока для интерфейса менеджера кеша
        _cacheManagerMock = new Mock<ICacheManager>();

        // Инициализация обработчика команды добавления пользователя
        _userCommandHandler = new AddUserCommandHandler(_userStoreMock.Object, _cacheManagerMock.Object);
    }

    /// <summary>
    /// Валидный тест на добавление пользователя в базу
    /// </summary>
    /// <param name="firstName">Имя</param>
    /// <param name="lastName">Фамилия</param>
    /// <param name="phone">Номер телефона</param>
    /// <param name="email">Электронная почта</param>
    /// <param name="patronymic">Отчество</param>
    [TestCase("Максим", "Завгородний", "+79586745866", "zvg@yandex.com", "Валерьевич")]
    [TestCase("Максим", "Завгородний", "+79586745866", "zvg@yandex.com", null)]
    public async Task Handle_ValidUser_AddsUserToStore(string firstName, string lastName, string phone, string email,
        string patronymic)
    {
        // Создаем команду добавления пользователя
        var command = new AddUserCommand
        {
            FirstName = firstName,
            LastName = lastName,
            Patronymic = patronymic,
            Email = email,
            PhoneNumber = phone
        };

        // Вызываем выполнение команды
        await _userCommandHandler.Handle(command, CancellationToken.None);

        // Убеждаемся, что в репозиторий был передан объект пользователя
        _userStoreMock.Verify(x => x.AddAsync(It.IsAny<UserAggregate>()), Times.Once);
    }

}