﻿using AddOrganizationInputModel = RequestManager.Presentation.Contracts.Organization.Validators.SettingsAddOrganizationInputModelValidator;
using AddUserInputModel = RequestManager.Presentation.Contracts.User.Validators.SettingsAddUserInputModelValidator;
using AddPostInputModel = RequestManager.Presentation.Contracts.Post.Validators.SettingsAddPostInputModelValidator;

namespace RequestManager.App.Settings;

/// <summary>
/// Статический класс для заполнения с конфига настроек валидации
/// </summary>
public static class AggregatesSettings
{
    /// <summary>
    /// Метод заполнения настроек валидации
    /// </summary>
    /// <param name="configuration">Конфигурация приложения</param>
    public static void AddValidationSettings(this IServiceCollection services, IConfiguration configuration)
    {
        // Загружаем из конфига все настройки агрегатов
        var settings = configuration.GetSection("ValidationSettings");

        // Загружаем настройки валидации добавления организаций
        var addOrganizationInputModelSettings = settings.GetSection("AddOrganizationInputModel");

        // Загружаем настройки валидации добавления постов
        var addPostInputModelSettings = settings.GetSection("AddPostInputModel");

        // Загружаем настройки валидации добавления пользователя
        var addUserInputModelSettings = settings.GetSection("AddUserInputModel");

        // Устанавливаем настройки валидации добавления огранизаций
        SetAddOrganizationInputModelSettings(addOrganizationInputModelSettings);

        // Устанавливаем настройки валидации добавления поста
        SetAddPostInputModelSettings(addPostInputModelSettings);

        // Устанавливаем настройки валидации добавления пользователя
        SetAddUserInputModelSettings(addUserInputModelSettings);
    }

    /// <summary>
    /// Метод установления настроек валидации добавления организаций
    /// </summary>
    /// <param name="section">Секция конфигурации</param>
    private static void SetAddOrganizationInputModelSettings(IConfigurationSection section)
    {
        // Устанавливаем максимальную длину названия организации
        AddOrganizationInputModel.MaxNameLength = section.GetValue<int>("MaxNameLength");

        // Устанавливаем минимальную длину названия организации
        AddOrganizationInputModel.MinNameLength = section.GetValue<int>("MinNameLength");
    }

    /// <summary>
    /// Метод установления настроек валидации добавления постов
    /// </summary>
    /// <param name="section">Секция конфигурации</param>
    private static void SetAddPostInputModelSettings(IConfigurationSection section)
    {
        // Устанавливаем максимальную длину описания
        AddPostInputModel.MaxDescLength = section.GetValue<int>("MaxDescLength");

        // Устанавливаем минимальную длину описания
        AddPostInputModel.MinDescLength = section.GetValue<int>("MinDescLength");

        // Получаем из конфига максимальный размер файла в байтах
        AddPostInputModel.MaxFileSize = section.GetValue<int>("MaxFileSize");

        // Получаем из конфига разрешенные форматы файла
        AddPostInputModel.ImageTypes = section.GetSection("ImageTypes").GetChildren().Select(c => c.Value).ToArray();
    }

    /// <summary>
    /// Метод установления настроек валидации добавления пользователя
    /// </summary>
    /// <param name="section">Секция конфигурации</param>
    private static void SetAddUserInputModelSettings(IConfigurationSection section)
    {
        // Устанавливаем максимальную длину имени
        AddUserInputModel.MaxFirstNameLength = section.GetValue<int>("MaxFirstNameLength");

        // Устанавливаем минимальную длину имени
        AddUserInputModel.MinFirstNameLength = section.GetValue<int>("MinFirstNameLength");

        // Устанавливаем максимальную длину фамилии
        AddUserInputModel.MaxLastNameLength = section.GetValue<int>("MaxLastNameLength");

        // Устанавливаем минимальную длину фамилии
        AddUserInputModel.MinLastNameLength = section.GetValue<int>("MinLastNameLength");

        // Устанавливаем максимальную длину отчества
        AddUserInputModel.MaxPatronymicLength = section.GetValue<int>("MaxPatronymicLength");

        // Устанавливаем минимальную длину отчества
        AddUserInputModel.MinPatronymicLength = section.GetValue<int>("MinPatronymicLength");
    }
}
