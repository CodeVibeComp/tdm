using FluentValidation;
using RequestManager.Presentation.Contracts.Organization;
using RequestManager.Presentation.Contracts.Organization.Validators;
using RequestManager.Presentation.Contracts.Post;
using RequestManager.Presentation.Contracts.Post.Validators;
using RequestManager.Presentation.Contracts.User;
using RequestManager.Presentation.Contracts.User.Validators;

namespace RequestManager.App.Extensions;

/// <summary>
/// Статический класс для регистрации сервиса FluentValidation в контейнере DI 
/// </summary>
public static class ValidationServices
{
    /// <summary>
    /// Метод регистрирует сервис FluentValidation в контейнере DI 
    /// </summary>
    /// <param name="services">Абстракция, которая представляет коллекцию сервисов (зависимостей),
    /// используемых в приложении.</param>
    public static void AddValidationServices(this IServiceCollection services)
    {
        // Регистрируем валидатор для AddUserInputModel
        services.AddScoped<IValidator<AddUserInputModel>, AddUserInputModelValidator>();

        // Регистрируем валидатор для AddOrganizationInputModel
        services.AddScoped<IValidator<AddOrganizationInputModel>, AddOrganizationInputModelValidator>();

        // Регистрируем валидатор для AddPostInputModel
        services.AddScoped<IValidator<AddPostInputModel>, AddPostInputModelValidator>();
    }
}