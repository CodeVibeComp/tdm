namespace UserService.Domain.Abstractions.Repositories;

/// <summary>
/// Интерфейс репозитория пользователя
/// </summary>
public interface IUserRepository
{
    /// <summary>
    /// Метод отдает пользователя по id
    /// </summary>
    /// <param name="id">Id пользователя</param>
    /// <returns>Отдает пользователя по id</returns>
    Task<Aggregates.User.UserAggregate?> GetAsync(Guid id);
    
    /// <summary>
    /// Метод обновляет данные пользователя
    /// </summary>
    /// <param name="user">Объект описывающий пользователя</param>
    Task UpdateAsync(Aggregates.User.UserAggregate user);
    
    /// <summary>
    /// Метод добавляет пользователя
    /// </summary>
    /// <param name="user">Объект описывающий пользователя</param>
    Task AddAsync(Aggregates.User.UserAggregate user);
    
    /// <summary>
    /// Метод находит пользователей в соответствии с указанной организацией
    /// </summary>
    /// <param name="organizationId">Id организации</param>
    /// <param name="skip">Количество записей которые нужно пропустить</param>
    /// <param name="take">Количество записей которые нужно получить</param>
    /// <returns>Коллекция пользователей удовлетворяющих запросу</returns>
    Task<ICollection<Aggregates.User.UserAggregate>> FindAsync(Guid? organizationId, int skip, int take);
    
    /// <summary>
    /// Метод находит пользователей без организации
    /// </summary>
    /// <param name="skip">Количество записей которые нужно пропустить</param>
    /// <param name="take">Количество записей которые нужно получить</param>
    /// <returns></returns>
    Task<ICollection<Aggregates.User.UserAggregate>> FindWithoutOrganizationAsync(int skip, int take);
}