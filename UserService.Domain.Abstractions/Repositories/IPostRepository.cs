﻿namespace UserService.Domain.Abstractions.Repositories;

/// <summary>
/// Интерфейс репозитория постов
/// </summary>
public interface IPostRepository
{
    /// <summary>
    /// Метод отдает пост по Id
    /// </summary>
    /// <param name="id">Id организации</param>
    /// <returns>Отдает организацию по Id</returns>
    Task<Aggregates.Post.PostAggregate?> GetAsync(Guid id);

    /// <summary>
    /// Метод обновляет данные поста
    /// </summary>
    /// <param name="user">Объект, описывающий пост</param>
    Task UpdateAsync(Aggregates.Post.PostAggregate post);

    /// <summary>
    /// Метод добавляет пост
    /// </summary>
    /// <param name="user">Объект, описывающий пост</param>
    Task AddAsync(Aggregates.Post.PostAggregate post);

    /// <summary>
    /// Метод находит посты
    /// </summary>
    /// <param name="skip">Количество записей которые нужно пропустить</param>
    /// <param name="take">Количество записей которые нужно получить</param>
    /// <returns>Коллекция постов удовлетворяющих запросу</returns>
    Task<ICollection<Aggregates.Post.PostAggregate>> FindAsync(int skip, int take);
}
