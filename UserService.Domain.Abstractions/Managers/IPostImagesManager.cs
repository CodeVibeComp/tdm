﻿namespace UserService.Domain.Abstractions.Managers;

///<summary>
///Интерфейс менеджера по управлению изображениями постов
///</summary>
public interface IPostImagesManager
{
    /// <summary>
    ///Метод добавления нового поста изображения
    ///</summary>
    ///<param name="filename">Название файла (исходное)</param>
    ///<param name="imageContent">Содержимое изображения</param>
    ///<returns>Отдает название файла в хранилище</returns>
    Task<string>AddAsync(string filename, byte[] imageContent);

    ///<summary>
    ///Метод получения изображения по его названию
    ///</summary>
    ///<param name="filename">Название файла (в хранилище)</param>
    ///<returns>Отдает ссылку на изображение</returns>
    Task<string> GetAsync(string filename);
}
