﻿namespace UserService.Domain.Abstractions.Managers;

/// <summary>
/// Интерфейс менеджера управлением кеша
/// </summary>
public interface ICacheManager
{
    /// <summary>
    /// Метод отдает запись по ключу из кеша
    /// </summary>
    /// <param name="key">Ключ поиска</param>
    /// <returns>Отдает объект по ключу</returns>
    Task<T> GetAsync<T>(string key);

    /// <summary>
    /// Метод добавляет запись в кеш
    /// </summary>
    /// <param name="key">Ключ поиска этого кеша</param>
    /// <param name="value">Объект, который нужно записать в кеш</param>
    /// <returns></returns>
    Task<T> AddAsync<T>(string key, T value);

    /// <summary>
    /// Метод удаляет запись из кеша
    /// </summary>
    /// <param name="key">Ключ поиска этого кеша</param>
    Task DeleteAsync(string key);
}
