﻿using UserService.Domain.Abstractions.Repositories;

namespace UserService.Domain.Abstractions.UnitOfWork;

/// <summary>
/// Интерфейс единицы работы с репозиториями
/// </summary>
public interface IUnitOfWork
{
    /// <summary>
    /// Интерфейс репозитория постов
    /// </summary>
    IPostRepository PostRepository { get; }

    /// <summary>
    /// Интерфейс репозитория организаций
    /// </summary>
    IOrganizationRepository OrganizationRepository { get; }

    /// <summary>
    /// Интерфейс репозитория пользователей
    /// </summary>
    IUserRepository UserRepository { get; }

    /// <summary>
    /// Метод сохраняет все изменения в базу данных
    /// </summary>
    Task SaveChangesAsync();
}