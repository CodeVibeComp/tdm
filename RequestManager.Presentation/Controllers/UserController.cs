using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using RequestManager.Application.Abstractions.Commands;
using RequestManager.Presentation.Contracts.User;

namespace RequestManager.Presentation.Controllers;

/// <summary>
/// Контроллер управления пользователями
/// </summary>
/// <param name="mediator">Интерефейс Посредник</param>
/// <param name="validatorAddUserInput">Интерфейс валидатора для AddUserInputModel</param>
[ApiController]
[Route("api/[controller]/[action]")]
public class UserController(ISender mediator, IValidator<AddUserInputModel> validatorAddUserInput)
    : ControllerBase
{
    /// <summary>
    /// Метод для отравки команды на добавление пользователя (fanout). 
    /// </summary>
    /// <param name="addUser">Модель для добавления пользователя</param>
    /// <param name="token">Токен отмены операции</param>
    /// <response code="200">Запрос выполнен успешно</response>
    /// <response code="400">Запрос не был выполнен, т.к. данные не валидны</response>
    [HttpPost]
    public async Task<ActionResult> AddUserByFanout(AddUserInputModel addUser, CancellationToken token)
    {
        // Получаем результат валидации
        var validationResult = await validatorAddUserInput.ValidateAsync(addUser, token);

        // Проверка валидности модели
        if (!validationResult.IsValid)
        {
            // Отправляем код 400
            return BadRequest(new
            {

                // Формируем список ошибок на основе валидации
                Error = validationResult.Errors.Select(x => $"{x.PropertyName}: {x.ErrorMessage}").ToArray()

            });
        }

        // Отправляем команду на добавление пользователя
        await mediator.Send(
            new AddUserCommand
            {
                FirstName = addUser.FirstName,
                LastName = addUser.LastName,
                PhoneNumber = addUser.PhoneNumber,
                Email = addUser.Email,
                Patronymic = addUser.Patronymic
            }, token);

        // Отправляем код 200
        return Ok();
    }

    /// <summary>
    /// Метод для отравки команды на добавление пользователя (direct). 
    /// </summary>
    /// <param name="addUser">Модель для добавления пользователя</param>
    /// <param name="token">Токен отмены операции</param>
    ///<response code="200">Запрос выполнен успешно</response>
    ///<response code="400">Запрос не был выполнен, т.к. данные не валидны</response>
    [HttpPost]
    public async Task<IActionResult> AddUserByDirect(AddUserInputModel addUser, CancellationToken token)
    {
        // Отправляем команду на добавление пользователя
        await mediator.Send(
            new AddUserByDirectCommand
            {
                FirstName = addUser.FirstName,
                LastName = addUser.LastName,
                PhoneNumber = addUser.PhoneNumber,
                Email = addUser.Email,
                Patronymic = addUser.Patronymic
            }, token);

        // Отправляем 200
        return Ok();
    }

    /// <summary>
    /// Метод для отравки команды на добавление пользователя (topic). 
    /// </summary>
    /// <param name="addUser">Модель для добавления пользователя</param>
    /// <param name="token">Токен отмены операции</param>
    ///<response code="200">Запрос выполнен успешно</response>
    ///<response code="400">Запрос не был выполнен, т.к. данные не валидны</response>
    [HttpPost]
    public async Task<IActionResult> AddUserByTopic(AddUserInputModel addUser, CancellationToken token)
    {
        // Получаем результат валидации
        var validationResult = await validatorAddUserInput.ValidateAsync(addUser, token);

        // Проверка валидности модели
        if (!validationResult.IsValid)
        {

            // Возвращаем ошибочный формат ответа со статусом 400
            return BadRequest(new
            {

                // Формируем ошибки на основе валидации модели
                Errors = validationResult.Errors.Select(x => $"{x.PropertyName}: {x.ErrorMessage}").ToArray()
            });
        }

        // Отправляем команду на добавление пользователя
        await mediator.Send(
            new AddUserByTopicCommand
            {
                FirstName = addUser.FirstName!,
                LastName = addUser.LastName!,
                PhoneNumber = addUser.PhoneNumber!,
                Email = addUser.Email!,
                Patronymic = addUser.Patronymic
            }, token);

        //Отправляем 200
        return Ok();
    }

    /// <summary>
    /// Метод для отравки команды на добавление пользователя (headers). 
    /// </summary>
    /// <param name="addUser">Модель для добавления пользователя</param>
    /// <param name="token">Токен отмены операции</param>
    /// <response code="200">Запрос выполнен успешно</response>
    /// <response code="400">Запрос не был выполнен, т.к. данные не валидны</response>
    [HttpPost]
    public async Task<IActionResult> AddUserByHeaders(AddUserInputModel addUser, CancellationToken token)
    {
        //получаем результат валидации
        var validationResult = await validatorAddUserInput.ValidateAsync(addUser, token);

        //проверка валидности модели
        if (!validationResult.IsValid)
        {
            // Возвращаем ошибочный формат ответа со статусом 400
            return BadRequest(new
            {
                // Формируем ошибки на основе валидации модели
                Errors = validationResult.Errors.Select(x => $"{x.PropertyName}: {x.ErrorMessage}").ToArray()
            });
        }
        
        // Отправляем команду на добавление пользователя
        await mediator.Send(
            new AddUserByHeadersCommand
            {
                FirstName = addUser.FirstName,
                LastName = addUser.LastName,
                PhoneNumber = addUser.PhoneNumber,
                Email = addUser.Email,
                Patronymic = addUser.Patronymic
            }, token);

        // Отправляем 200
        return Ok();
    }
}