﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using RequestManager.Application.Abstractions.Commands;
using RequestManager.Presentation.Contracts.Post;

namespace RequestManager.Presentation.Controllers;

/// <summary>
/// Контроллер управления постами
/// </summary>
/// <param name="mediator">Интерефейс Посредник</param>
/// <param name="addPostInputModelValidator">Интерфейс валидатора для addPostInputModelValidator</param>
[ApiController]
[Route("api/[controller]/[action]")]
public class PostController(ISender mediator, IValidator<AddPostInputModel> addPostInputModelValidator): ControllerBase
{
    /// <summary>
    /// Метод для отравки команды на добавление нового поста. 
    /// </summary>
    /// <param name="addUser">Входная модель для добавления поста</param>
    /// <param name="token">Токен отмены</param>
    /// <response code="400">Запрос не прошел валидацию</response>
    /// <response code="200">Запрос выполнен успешно</response>
    [HttpPost]
    public async Task<ActionResult> AddPost([FromForm] AddPostInputModel addUser, CancellationToken token)
    {
        // Получаем результат валидации полученной модели
        var validationResult = await addPostInputModelValidator.ValidateAsync(addUser, token);

        // Проверка на валидность модели
        if (!validationResult.IsValid)
        {

            // Возвращаем BadRequest (StatusCode: 400)
            return BadRequest(new
            {

                // Формируем список ошибок по результатам валидации
                Error = validationResult.Errors.Select(x => $"{x.PropertyName}: {x.ErrorMessage}").ToArray()

            });
        }

        // Создаем новый MemoryStream
        await using var imageFileStream = new MemoryStream();

        // Копируем содержимое изображения в MemoryStream
        await addUser.Image.CopyToAsync(imageFileStream);

        // Отправляем команду на добавление поста
        await mediator.Send(
            new AddPostCommand
            {
                ImageName = addUser.Image.FileName,
                ImageContent = imageFileStream.ToArray(),
                Description = addUser.Description,
            }, token);

        // Возвращаем 200 статус
        return Ok();
    }
}
