using FluentValidation;
using Settings = RequestManager.Presentation.Contracts.Organization.Validators.SettingsAddOrganizationInputModelValidator;

namespace RequestManager.Presentation.Contracts.Organization.Validators;

/// <summary>
/// Валидатор для AddOrganizationInputModel
/// </summary>
public class AddOrganizationInputModelValidator:AbstractValidator<AddOrganizationInputModel>
{
    /// <summary>
    /// Правила валидации
    /// </summary>
    public AddOrganizationInputModelValidator()
    {
        // Настраиваем правила валидации названия организации
        RuleFor(e => e.Name)
            .NotEmpty().WithMessage("Название является обязательным полем.")
            .MinimumLength(Settings.MinNameLength).WithMessage("Название должно составлять как минимум 3 буквы.")
            .MaximumLength(Settings.MaxNameLength).WithMessage("Название не может превышать 50 символов.");
    }
}