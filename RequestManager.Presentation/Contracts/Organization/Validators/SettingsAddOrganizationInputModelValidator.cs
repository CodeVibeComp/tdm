﻿namespace RequestManager.Presentation.Contracts.Organization.Validators;

/// <summary>
/// Параметры валидации модели добавления организации
/// </summary>
public static class SettingsAddOrganizationInputModelValidator
{
    /// <summary>
    /// Максимальное количество символов в названии
    /// </summary>
    public static int MaxNameLength { get; set; }

    /// <summary>
    /// Минимальное количество символов в названии
    /// </summary>
    public static int MinNameLength { get; set; }
}
