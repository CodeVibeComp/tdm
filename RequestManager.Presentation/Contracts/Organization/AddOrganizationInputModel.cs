namespace RequestManager.Presentation.Contracts.Organization;

/// <summary>
/// Входная модель добавления организации для контроллера
/// </summary>
public class AddOrganizationInputModel
{
    // Название организации
    public string Name { get; set; }
}