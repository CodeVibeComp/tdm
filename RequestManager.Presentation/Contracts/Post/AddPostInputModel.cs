﻿using Microsoft.AspNetCore.Http;

namespace RequestManager.Presentation.Contracts.Post;

/// <summary>
/// Входная модель добавления поста для контроллера
/// </summary>
/// <param name="Image">Изображение</param>
/// <param name="Description">Описание</param>
public class AddPostInputModel
{
    // Загруженное пользователем изображение
    public IFormFile Image { get; set; }

    // Описание полученного поста
    public string Description { get; set; }
}