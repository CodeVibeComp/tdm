﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Settings = RequestManager.Presentation.Contracts.Post.Validators.SettingsAddPostInputModelValidator;

namespace RequestManager.Presentation.Contracts.Post.Validators;

/// <summary>
/// Валидатор для AddPostInputModel
/// </summary>
public class AddPostInputModelValidator : AbstractValidator<AddPostInputModel>
{
    /// <summary>
    /// Правила валидации
    /// </summary>
    public AddPostInputModelValidator()
    {
        // Устанавливаем правило валидации описания
        RuleFor(e => e.Description)
            .NotEmpty().WithMessage("Описание поста является обязательным полем.")
            .MinimumLength(Settings.MinDescLength).WithMessage("Описание поста должно составлять как минимум 5 символов.")
            .MaximumLength(Settings.MaxDescLength).WithMessage("Описание поста не может превышать 350 символов.");

        // Устанавливаем правило валидации изображения
        RuleFor(e => e.Image)
            .Must(BeValidImage).WithMessage($"Изображение должно быть обязательным полем формата {string.Join(", ", Settings.ImageTypes)}");
    }

    /// <summary>
    /// Метод проверки файла на правильность полученного изображения
    /// </summary>
    /// <param name="model">Модель</param>
    /// <param name="image">Файл IFormFile</param>
    /// <returns>Валидное изображение или нет</returns>
    private bool BeValidImage(AddPostInputModel model, IFormFile image)
    {
        // Проверка на пустое изображение 
        if (image == null)
            return false;

        // Проверка на превышение размера файла
        if (image.Length > Settings.MaxFileSize)
            return false;

        // Приводим путь к изображению в нижний регистр
        var extension = Path.GetExtension(image.FileName).ToLower();

        // Проверка на соответствие разрешенному формату файла
        if (!Settings.ImageTypes.Contains(extension))
            return false;

        // Если проверка пройдена успешно валидируем
        return true;
    }
}
