﻿namespace RequestManager.Presentation.Contracts.Post.Validators;

/// <summary>
/// Параметры валидации модели добавления пользователя
/// </summary>
public static class SettingsAddPostInputModelValidator
{
    /// <summary>
    /// Максимальный размер изображения
    /// </summary>
    public static int MaxFileSize { get; set; }

    /// <summary>
    /// Разрешенные форматы файла
    /// </summary>
    public static string[] ImageTypes { get; set; }

    /// <summary>
    /// Максимальное количество символов в описании
    /// </summary>
    public static int MinDescLength { get; set; }

    /// <summary>
    /// Минимальное количество символов в описании
    /// </summary>
    public static int MaxDescLength { get; set; }
}
