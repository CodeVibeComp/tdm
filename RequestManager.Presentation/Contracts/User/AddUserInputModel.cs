namespace RequestManager.Presentation.Contracts.User;

/// <summary>
/// Входная модель добавления пользователя для контроллера
/// </summary>
/// <param name="FirstName">Имя</param>
/// <param name="LastName">Фамилия</param>
/// <param name="Patronymic">Отчество</param>
/// <param name="PhoneNumber">Номер телефона</param>
/// <param name="Email">Электронная почта</param>
public class AddUserInputModel
{
    // Имя пользователя
    public string FirstName { get; set; }

    // Фамилия пользователя
    public string LastName { get; set; }

    // Отчество пользователя
    public string? Patronymic { get; set; }

    // Номер телефона
    public string PhoneNumber { get; set; }

    // Адрес электронной почты
    public string Email { get; set; } 
}