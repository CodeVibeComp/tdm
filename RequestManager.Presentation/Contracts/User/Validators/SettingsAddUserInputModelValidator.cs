﻿using System.Text.RegularExpressions;

namespace RequestManager.Presentation.Contracts.User.Validators;

/// <summary>
/// Параметры валидации модели добавления пользователя
/// </summary>
public static class SettingsAddUserInputModelValidator
{
    /// <summary>
    /// Регулярный выражение для проверки номера телефона
    /// </summary>
    public static Regex RegexPhoneNumber => new(@"^\+\d{11}$");

    /// <summary>
    /// Максимальное количество символов в имени
    /// </summary>
    public static int MaxFirstNameLength { get; set; }

    /// <summary>
    /// Минимальное количество символов в имени
    /// </summary>
    public static int MinFirstNameLength { get; set; }

    /// <summary>
    /// Максимальное количество символов в фамилии
    /// </summary>
    public static int MaxLastNameLength { get; set; }

    /// <summary>
    /// Минимальное количество символов в фамилии
    /// </summary>
    public static int MinLastNameLength { get; set; }

    /// <summary>
    /// Максимальное количество символов в отчестве
    /// </summary>
    public static int MaxPatronymicLength { get; set; }

    /// <summary>
    /// Минимальное количество символов в отчестве
    /// </summary>
    public static int MinPatronymicLength { get; set; }
}
