using FluentValidation;
using Settings = RequestManager.Presentation.Contracts.User.Validators.SettingsAddUserInputModelValidator;

namespace RequestManager.Presentation.Contracts.User.Validators;

/// <summary>
/// Валидатор для AddUserInputModel
/// </summary>
public class AddUserInputModelValidator: AbstractValidator<AddUserInputModel>
{
    /// <summary>
    /// Правила валидации
    /// </summary>
    public AddUserInputModelValidator()
    {
        // Настраиваем правила имени пользователя
        RuleFor(x => x.FirstName)
            .NotEmpty().WithMessage("Имя является обязательным полем.")
            .MinimumLength(Settings.MinFirstNameLength).WithMessage("Имя должно составлять как минимум 3 буквы.")
            .MaximumLength(Settings.MaxFirstNameLength).WithMessage("Имя не может превышать 50 символов.");

        // Настраиваем правила фамилии пользователя
        RuleFor(x => x.LastName)
            .NotEmpty().WithMessage("Фамилия является обязательным полем.")
            .MinimumLength(Settings.MinLastNameLength).WithMessage("Фамилия должна составлять как минимум 3 буквы.")
            .MaximumLength(Settings.MaxLastNameLength).WithMessage("Фамилия не может превышать 50 символов.");

        // Настраиваем правила отчества пользователя
        RuleFor(x => x.Patronymic)
            .MinimumLength(Settings.MinPatronymicLength).WithMessage("Отчество должно составлять как минимум 3 буквы.")
            .MaximumLength(Settings.MaxPatronymicLength).WithMessage("Отчество не может превышать 50 символов.");

        // Настраиваем правила формата номера телефона
        RuleFor(x => x.PhoneNumber)
            .NotEmpty().WithMessage("Номер телефона является обязательным полем.")
            .Matches(Settings.RegexPhoneNumber).WithMessage("Номер телефона должен быть в формате +XXXXXXXXXXX, где X - цифра.");

        // Настраиваем правила формата адреса электронной почты
        RuleFor(x => x.Email)
            .NotEmpty().WithMessage("Email является обязательным полем.")
            .EmailAddress().WithMessage("Неверный формат Email.");
    }
}