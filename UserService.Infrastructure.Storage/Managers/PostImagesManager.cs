﻿using Microsoft.Extensions.Configuration;
using Minio;
using Minio.DataModel.Args;
using UserService.Domain.Abstractions.Managers;

namespace UserService.Infrastructure.Storage.Managers;

/// <summary>
/// Реализация интерфейса взаимодейтсвия с изображениями постов
/// </summary>
public class PostImagesManager : IPostImagesManager
{
    // Интерфейс клиента MinIO
    private readonly IMinioClient _minioClient;

    // Интерфейс конфигурации проекта
    private readonly IConfiguration _configuration;

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="minioClient">Клиент minIO</param>
    public PostImagesManager(IConfiguration configuration, IMinioClient minioClient)
    {
        _minioClient = minioClient;
        _configuration = configuration;
    }

    /// <summary>
    /// Асинхронное добавление нового изображения
    /// </summary>
    /// <param name="filename">Название файла</param>
    /// <param name="value">Объект для записи</param>
    /// <returns>Название файла в хранилище</returns>
    public async Task<string> AddAsync(string filename, byte[] imageContent)
    {
        // Формат файла
        var extension = Path.GetExtension(filename).TrimStart('.');

        // Название файла в хранилище
        var objectName = $"{Guid.NewGuid()}.{extension}";

        // MemoryStream содержимого изображения
        using var stream = new MemoryStream(imageContent);

        // Получаем название хранилища в Minio из настроек приложения
        var bukkitName = _configuration["Minio:PostBukkit"];
        
        // Параметры загрузки изображения
        var putObjectArgs = new PutObjectArgs()
            .WithBucket(bukkitName)
            .WithObject(objectName)
            .WithStreamData(stream)
            .WithContentType($"image/{extension}")
            .WithObjectSize(stream.Length);

        // Асинхронная загрузка файла
        var result = await _minioClient.PutObjectAsync(putObjectArgs);

        // Возвращаем название файла в хранилище
        return result.ObjectName;
    }

    /// <summary>
    /// Асинхронное получение изображения
    /// </summary>
    /// <param name="filename">Название файла</param>
    /// <returns>Ссылка на изображение</returns>
    public async Task<string> GetAsync(string filename)
    {
        // Получаем базовую ссылку на файловый сервер из настроек приложения
        var baseUrl = _configuration["Minio:Url"];
        
        // Получаем название хранилища в Minio из настроек приложения
        var bukkitName = _configuration["Minio:PostBukkit"];
        
        return $"{baseUrl}/{bukkitName}/{filename}";
    }
}
