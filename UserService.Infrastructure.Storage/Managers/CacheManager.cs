﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using UserService.Domain.Abstractions.Managers;
using UserService.Infrastructure.Storage.Context;

namespace UserService.Infrastructure.Storage.Managers;

/// <summary>
/// Реализация интерфейса ICacheManager
/// </summary>
public class CacheManager : ICacheManager
{
    /// <summary>
    /// Интерфейс взаимодейтсвия с распределенным кешом
    /// </summary>
    private readonly IDistributedCache _cache;

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="context">Контекст базы данных</param>
    public CacheManager(IDistributedCache cache)
    {
        _cache = cache;
    }

    /// <summary>
    /// Асинхронное добавление записи в кеш
    /// </summary>
    /// <param name="key">Ключ поиска</param>
    /// <param name="value">Объект для записи</param>
    /// <returns>Отдает объект для записи</returns>
    public async Task<T> AddAsync<T>(string key, T value)
    {
        // Запись объекта в кеш
        await _cache.SetStringAsync(key, JsonConvert.SerializeObject(value), new DistributedCacheEntryOptions
        {
            AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(1),
            SlidingExpiration = TimeSpan.FromMinutes(10)
        });

        // Возврат записи
        return value;
    }

    /// <summary>
    /// Асинхронный поиск записей в кеше
    /// </summary>
    /// <param name="key">Ключ поиска</param>
    /// <returns>Отдает объект по ключу</returns>
    public async Task<T> GetAsync<T>(string key)
    {
        // получаем значение по ключу key
        var value = await _cache.GetStringAsync(key);

        // если значение есть преобразовываем в объект и возвращаем
        if (value != null)
        {
            return JsonConvert.DeserializeObject<T>(value);
        }

        // если не нашли, возвращаем значение объекта T по умолчанию
        return default; 
    }

    /// <summary>
    /// Асинхронное удаление записи из кеша
    /// </summary>
    /// <param name="key">Ключ записи</param>
    public async Task DeleteAsync(string key)
    {
        // получаем значение по ключу key и удаляем его из кеша
        await _cache.RemoveAsync(key);
    }
}
