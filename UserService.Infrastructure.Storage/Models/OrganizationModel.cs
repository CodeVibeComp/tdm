using System.ComponentModel.DataAnnotations;

namespace UserService.Infrastructure.Storage.Models;

public class OrganizationModel
{
    /// <summary>
    /// Идентификатор
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Название Организации
    /// </summary>
    [MaxLength(100)] public string Name { get; set; } = null!;

    /// <summary>
    /// Список пользоватей входящие в данную организацию
    /// </summary>
    public List<UserModel> Users { get; set; } = [];
}