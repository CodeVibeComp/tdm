using System.ComponentModel.DataAnnotations;

namespace UserService.Infrastructure.Storage.Models;

public class UserModel
{
    /// <summary>
    /// Идентификатор пользователя
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Идентификатор организации
    /// </summary>
    public Guid? OrganizationId { get; set; }

    /// <summary>
    /// Cсылка на организацию
    /// </summary>
    public OrganizationModel? Organization { get; set; }

    /// <summary>
    /// Имя
    /// </summary>
    [MaxLength(50)] public string FirstName { get; set; } = null!;

    /// <summary>
    /// Фамилия
    /// </summary>
    [MaxLength(50)] public string LastName { get; set; } = null!;

    /// <summary>
    /// Отчество
    /// </summary>
    [MaxLength(50)] public string? Patronymic { get; set; }
    
    /// <summary>
    /// Номер телефона
    /// </summary>
    [MaxLength(50)] public string PhoneNumber { get; set; } = null!;
    
    /// <summary>
    /// Адрес электронной почты
    /// </summary>
    [MaxLength(50)] public string Email { get; set; } = null!;
}