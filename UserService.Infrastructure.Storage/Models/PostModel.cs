﻿using System.ComponentModel.DataAnnotations;

namespace UserService.Infrastructure.Storage.Models;

/// <summary>
/// Модель поста
/// </summary>
public class PostModel
{
    /// <summary>
    /// Идентификатор поста
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Идентификатор изображения в храналище
    /// </summary>
    public string ImageName { get; set; }

    /// <summary>
    /// Описание поста
    /// </summary>
    [MaxLength(350)] public string Description { get; set; }
}
