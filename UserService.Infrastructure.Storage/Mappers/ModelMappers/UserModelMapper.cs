using AutoMapper;
using UserService.Domain.Aggregates.User;
using UserService.Infrastructure.Storage.Models;

namespace UserService.Infrastructure.Storage.Mappers.ModelMappers;

/// <summary>
/// Класс содержит метод преобразования агрегата в модель организации
/// </summary>
public class UserModelMapper
{
    // Интерфейс сервиса IMapper
    private readonly IMapper _mapper;

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="mapper">Интерфейс сервиса IMapper</param>
    public UserModelMapper(IMapper mapper)
    {
        _mapper = mapper;
    }

    /// <summary>
    /// Метод преобразует агрегат пользователя в модель пользователя
    /// </summary>
    /// <param name="userAggregate">Агрегат пользователя</param>
    /// <param name="userModel">Модель пользователя</param>
    /// <returns></returns>
    public UserModel Map(UserAggregate userAggregate)
    {
        return _mapper.Map<UserModel>(userAggregate);
    }
}