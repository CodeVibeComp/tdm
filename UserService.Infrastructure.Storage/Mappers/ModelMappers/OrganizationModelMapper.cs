using AutoMapper;
using UserService.Domain.Aggregates.Organization;
using UserService.Infrastructure.Storage.Models;

namespace UserService.Infrastructure.Storage.Mappers.ModelMappers;

/// <summary>
/// Класс содержит метод преобразования агрегата в модель организации
/// </summary>
public class OrganizationModelMapper
{
    // Интерфейс сервиса IMapper
    private readonly IMapper _mapper;

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="mapper">Интерфейс сервиса IMapper</param>
    public OrganizationModelMapper(IMapper mapper)
    {
        _mapper = mapper;
    }

    /// <summary>
    /// Метод преобразует агрегат организации в модель организации
    /// </summary>
    /// <param name="organizationAggregate">Агрегат организации</param>
    /// <param name="organizationModel">Модель организации</param>
    /// <returns></returns>
    public OrganizationModel Map(OrganizationAggregate organizationAggregate)
    {
        return _mapper.Map<OrganizationModel>(organizationAggregate);
    }
}
