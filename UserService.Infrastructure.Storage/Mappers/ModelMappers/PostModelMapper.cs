﻿using AutoMapper;
using UserService.Domain.Aggregates.Post;
using UserService.Infrastructure.Storage.Models;

namespace UserService.Infrastructure.Storage.Mappers.ModelMappers;

/// <summary>
/// Класс содержит метод преобразования агрегата в модель поста
/// </summary>
public class PostModelMapper
{
    // Интерфейс сервиса IMapper
    private readonly IMapper _mapper;

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="mapper">Интерфейс сервиса IMapper</param>
    public PostModelMapper(IMapper mapper)
    {
        _mapper = mapper;
    }

    /// <summary>
    /// Метод преобразует агрегат поста в модель поста
    /// </summary>
    /// <param name="postAggregate">Агрегат поста</param>
    /// <param name="postModel">Модель поста</param>
    /// <returns></returns>
    public PostModel Map(PostAggregate postAggregate)
    {
        return _mapper.Map<PostModel>(postAggregate);
    }
}
