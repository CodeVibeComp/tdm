using AutoMapper;
using UserService.Domain.Aggregates.User;
using UserService.Infrastructure.Storage.Models;

namespace UserService.Infrastructure.Storage.Mappers.AggregateMappers;

/// <summary>
/// Класс содержит метод преобразования модели пользователя в агрегат
/// </summary>
public class UserMapper
{
    // Интерфейс сервиса IMapper
    private readonly IMapper _mapper;

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="mapper">Интерфейс сервиса IMapper</param>
    public UserMapper(IMapper mapper)
    {
        _mapper = mapper;
    }

    /// <summary>
    /// Метод преобразует модель пользователя в агрегат
    /// </summary>
    /// <param name="model">Модель пользователя</param>
    /// <returns>Агрегат пользователя</returns>
    public UserAggregate Map(UserModel model)
    {
        return _mapper.Map<UserAggregate>(model);
    }
}