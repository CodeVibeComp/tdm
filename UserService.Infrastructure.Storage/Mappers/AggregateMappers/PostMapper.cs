﻿using AutoMapper;
using UserService.Domain.Aggregates.Post;
using UserService.Infrastructure.Storage.Models;

namespace UserService.Infrastructure.Storage.Mappers.AggregateMappers;

/// <summary>
/// Класс содержит метод преобразования модели поста в его агрегат
/// </summary>
public class PostMapper
{
    // Интерфейс сервиса IMapper
    private readonly IMapper _mapper;

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="mapper">Интерфейс сервиса IMapper</param>
    public PostMapper(IMapper mapper)
    {
        _mapper = mapper;
    }

    /// <summary>
    /// Метод преобразует модель поста в агрегат
    /// </summary>
    /// <param name="model">Модель поста</param>
    /// <returns>Агрегат поста</returns>
    public PostAggregate Map(PostModel model)
    {
        return _mapper.Map<PostAggregate>(model);
    }
}