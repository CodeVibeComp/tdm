using AutoMapper;
using UserService.Domain.Aggregates.Organization;
using UserService.Infrastructure.Storage.Models;

namespace UserService.Infrastructure.Storage.Mappers.AggregateMappers;

/// <summary>
/// Класс содержит метод преобразования модели организации в агрегат
/// </summary>
public class OrganizationMapper
{
    // Интерфейс сервиса IMapper
    private readonly IMapper _mapper;

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="mapper">Интерфейс сервиса IMapper</param>
    public OrganizationMapper(IMapper mapper)
    {
        _mapper = mapper;
    }

    /// <summary>
    /// Метод преобразует модель организации в агрегат
    /// </summary>
    /// <param name="model">Модель организации</param>
    /// <returns>Агрегат организации</returns>
    public OrganizationAggregate Map(OrganizationModel model)
    {
        return _mapper.Map<OrganizationAggregate>(model);
    }
}