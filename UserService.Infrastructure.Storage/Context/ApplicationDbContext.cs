using Microsoft.EntityFrameworkCore;
using UserService.Infrastructure.Storage.Models;

namespace UserService.Infrastructure.Storage.Context;

/// <summary>
/// Контекст базы данных
/// </summary>
/// <param name="options">Опции для создания контекста</param>
public class ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : DbContext(options)
{
    /// <summary>
    /// Таблица пользователей
    /// </summary>
    public DbSet<UserModel> Users { get; set; } = null!;

    /// <summary>
    /// Таблица организаий
    /// </summary>
    public DbSet<OrganizationModel> Organizations { get; set; } = null!;

    /// <summary>
    /// Таблица постов
    /// </summary>
    public DbSet<PostModel> Posts { get; set; } = null!;

    /// <summary>
    /// Настройка базы данных при создании
    /// </summary>
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Настройка связей между таблицами
        modelBuilder.Entity<OrganizationModel>()
            .HasMany(x => x.Users)
            .WithOne(x => x.Organization)
            .HasForeignKey(u => u.OrganizationId).OnDelete(DeleteBehavior.Cascade);
    }
}