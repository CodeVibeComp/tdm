using AutoMapper;
using Microsoft.EntityFrameworkCore;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.Aggregates.User;
using UserService.Infrastructure.Storage.Context;
using UserService.Infrastructure.Storage.Mappers.AggregateMappers;
using UserService.Infrastructure.Storage.Mappers.ModelMappers;
using UserService.Infrastructure.Storage.Models;

namespace UserService.Infrastructure.Storage.Repositories;

/// <summary>
/// Реализация интерфейса IUserRepository
/// </summary>
public class UserRepository : IUserRepository
{
    /// <summary>
    /// Контекст базы данных
    /// </summary>
    private readonly ApplicationDbContext _context;

    /// <summary>
    /// Интерфейс автомаппера
    /// </summary>
    private readonly IMapper _mapper;

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="context">Контекст базы данных</param>
    /// <param name="mapper">Интерфейс автомаппера</param>
    public UserRepository(ApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    /// <summary>
    /// Получение пользователя по идентификатору асинхронно
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <returns>Агрегат пользователя</returns>
    public async Task<UserAggregate?> GetAsync(Guid id)
    {
        //Получение пользователя по id
        var userModel = await _context.Users
            .AsNoTracking()
            .Include(u => u.Organization)
            .FirstOrDefaultAsync(u => u.Id == id);
        
        //В случае отсутствия пользователя возвращаем null, иначе возвращаем агрегат пользователя
        return userModel == null ? null : _mapper.Map<UserAggregate>(userModel);
    }

    /// <summary>
    /// Обновление пользователя асинхронно
    /// </summary>
    /// <param name="user">Агрегат пользователя</param>
    public async Task UpdateAsync(UserAggregate user)
    {
        //Получение пользователя по id
        var userModel = await _context.Users
            .FirstOrDefaultAsync(u => u.Id == user.Id);
        
        //Если пользователь не найден выходим из метода
        if (userModel == null) return;

        // Преобразование агрегата пользователя в модель
        var u = _mapper.Map(user, userModel);
        
        //Обновление пользователя в бд
        _context.Users.Update(u);
        
        //Cохранение изменений в бд
        await _context.SaveChangesAsync();
    }

    /// <summary>
    /// Добавление пользователя асинхронно
    /// </summary>
    /// <param name="user">Агрегат пользователя</param>
    public async Task AddAsync(UserAggregate user)
    {
        //Создание модели нового пользователя
        var newUserModel = new UserModel { Id = user.Id };
        
        //Преобразование агрегата пользователя в модель
        var result = _mapper.Map(user, newUserModel);
        
        //Добавление пользователя в бд
        _context.Users.Add(result);
        
        //Сохранение изменений в бд
        await _context.SaveChangesAsync();
    }

    /// <summary>
    /// Поиск пользователей в соответствии с указанной организацией
    /// </summary>
    /// <param name="organizationId">Идентификатор организации</param>
    /// <param name="skip">Количество записей которые нужно пропустить</param>
    /// <param name="take">Количество записей которые нужно взять</param>
    /// <returns>Список агрегатов пользователей</returns>
    public async Task<ICollection<UserAggregate>> FindAsync(Guid? organizationId, int skip, int take)
    {
        //Получение списка пользователей в соответствии с идентификатором организации
        var users = await _context.Users.Where(u => u.OrganizationId == organizationId)
            .OrderBy(u => u.Id)
            .Skip(skip)
            .Take(take)
            .ToListAsync();
        
        //Преобразование списка моделей в cписок агрегатов пользователей
        return users.Select(u => _mapper.Map<UserAggregate>(u)).ToList();
    }

    /// <summary>
    /// Поиск пользователей без организации
    /// </summary>
    /// <param name="skip">Количество записей которые нужно пропустить</param>
    /// <param name="take">Количество записей которые нужно взять</param>
    /// <returns>Cписок агрегатов пользователей</returns>
    public async Task<ICollection<UserAggregate>> FindWithoutOrganizationAsync(int skip, int take)
    {
        //Получение списка пользователей без организации
        var users = await _context.Users.Where(u => u.OrganizationId == null)
            .OrderBy(u => u.Id)
            .Skip(skip)
            .Take(take)
            .ToListAsync();
        
        //Преобразование списка моделей в cписок агрегатов пользователей
        return users.Select(u => _mapper.Map<UserAggregate>(u)).ToList();
    }
}