﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.Aggregates.Post;
using UserService.Domain.Aggregates.PostAggregate;
using UserService.Infrastructure.Storage.Context;
using UserService.Infrastructure.Storage.Mappers.AggregateMappers;
using UserService.Infrastructure.Storage.Mappers.ModelMappers;
using UserService.Infrastructure.Storage.Models;

namespace UserService.Infrastructure.Storage.Repositories;

/// <summary>
/// Реализация интерфейса IPostRepository
/// </summary>
public class PostRepository : IPostRepository
{
    /// <summary>
    /// Контекст базы данных
    /// </summary>
    private readonly ApplicationDbContext _context;

    /// <summary>
    /// Интерфейс автомаппера
    /// </summary>
    private readonly IMapper _mapper;

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="context">Контекст базы данных</param>
    /// <param name="mapper">Интерфейс автомаппера</param>
    public PostRepository(ApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    /// <summary>
    /// Асинхронное получение поста по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <returns>Агрегат поста</returns>
    public async Task<PostAggregate?> GetAsync(Guid id)
    {
        // Находим пост по id
        var postModel = await _context.Posts
            .AsNoTracking()
            .FirstOrDefaultAsync(u => u.Id == id);

        // Возвращаем результат
        return postModel == null ? null : _mapper.Map<PostAggregate>(postModel);
    }

    /// <summary>
    /// Асинхронное добавление поста
    /// </summary>
    /// <param name="post">Агрегат поста</param>
    public async Task AddAsync(PostAggregate post)
    {
        // Создали модель поста
        var newPostModel = new PostModel { Id = post.Id };

        // Преобразование агрегата поста в модель
        var result = _mapper.Map(post, newPostModel);

        // Добавление поста в бд
        _context.Posts.Add(result);

        // Сохранение изменений в бд
        await _context.SaveChangesAsync();
    }

    /// <summary>
    /// Асинхронное обновление (изменение) поста
    /// </summary>
    /// <param name="post">Агрегат поста</param>
    public async Task UpdateAsync(PostAggregate post)
    {
        // Получение поста по id
        var postModel = await _context.Posts
            .FirstOrDefaultAsync(u => u.Id == post.Id);

        // Если пост не найден выходим из метода
        if (postModel == null) return;

        // Преобразование агрегата поста в модель
        var p = _mapper.Map(post, postModel);

        // Обновление пользователя в бд
        _context.Posts.Update(p);

        // Cохранение изменений в бд
        await _context.SaveChangesAsync();
    }

    /// <summary>
    /// Асинхронный поиск постов по параметрам
    /// </summary>
    /// <param name="skip">Количество записей которые нужно пропустить</param>
    /// <param name="take">Количество записей которые нужно получить</param>
    /// <returns>Список агрегатов постов</returns>
    public async Task<ICollection<PostAggregate>> FindAsync(int skip, int take)
    {
        // Получние списка постов
        var posts = await _context.Posts
            .OrderBy(o => o.Id)
            .Skip(skip)
            .Take(take)
            .ToListAsync();

        // Преобразование списка моделей в cписок агрегатов постов
        return posts
            .Select(p => _mapper.Map<PostAggregate>(p))
            .ToList();
    }
}