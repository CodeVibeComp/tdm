﻿using AutoMapper;
using UserService.Domain.Abstractions.Repositories;
using UserService.Domain.Abstractions.UnitOfWork;
using UserService.Infrastructure.Storage.Context;
using UserService.Infrastructure.Storage.Repositories;

namespace UserService.Infrastructure.Storage.UnitOfWork;

/// <summary>
/// Реализация интерфейса для работы с репозиториями базы данных
/// </summary>
public class UnitOfWork : IUnitOfWork
{
    /// <summary>
    /// Контекст базы данных
    /// </summary>
    private readonly ApplicationDbContext _context;

    /// <summary>
    /// Интерфейс автомаппера
    /// </summary>
    private readonly IMapper _mapper;

    /// <summary>
    /// Интерфейс репозитория организаций
    /// </summary>
    private IOrganizationRepository _organizationRepository;

    /// <summary>
    /// Интерфейс репозитория пользователей
    /// </summary>
    private IUserRepository _userRepository;

    /// <summary>
    /// Интерфейс репозитория постов
    /// </summary>
    private IPostRepository _postRepository;

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="context">Контекст базы данных</param>
    public UnitOfWork(ApplicationDbContext context, IMapper mapper)
    {
        // Загружаем контекст базы данных
        _context = context;
        
        // Загружаем маппер  
        _mapper = mapper;
    }

    /// <summary>
    /// Объект репозитория работы с постами
    /// </summary>
    public IPostRepository PostRepository
    {
        // При получении объекта 
        get
        {
            // Если _postRepository равен null, то создаем новый экзмемпляр
            _postRepository ??= new PostRepository(_context, _mapper);
            
            // Возвращаем полученный репозиторий
            return _postRepository;
        }
    }

    /// <summary>
    /// Объект репозитория работы с организациями
    /// </summary>
    public IOrganizationRepository OrganizationRepository
    {
        // При получении объекта 
        get
        {
            // Если _organizationRepository равен null, то создаем новый экзмемпляр
            _organizationRepository ??= new OrganizationRepository(_context, _mapper);
            
            // Возвращаем полученный репозиторий
            return _organizationRepository;
        }
    }

    /// <summary>
    /// Объект репозитория работы с пользователями
    /// </summary>
    public IUserRepository UserRepository
    {
        // При получении объекта 
        get
        {
            // Если _userRepository равен null, то создаем новый экзмемпляр
            _userRepository ??= new UserRepository(_context, _mapper);
            
            // Возвращаем полученный репозиторий
            return _userRepository;
        }
    }

    /// <summary>
    /// Сохранение изменений в базу данных
    /// </summary>
    public async Task SaveChangesAsync()
    {
        // Сохраняем контекст базы данных со всеми измененными сущностями
        await _context.SaveChangesAsync();
    }
}
