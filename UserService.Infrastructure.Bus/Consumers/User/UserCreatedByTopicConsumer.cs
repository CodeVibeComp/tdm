using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using Shared.Models;
using UserService.Application.Abstractions.Commands;

namespace UserService.Infrastructure.Bus.Consumers.User;

/// <summary>
/// Класс подписчик на очередь сообщений (добавление пользователя). ExchangeType - topic 
/// </summary>
/// <param name="mediator">Интерфейс посредника</param>
/// <param name="logger">Интерфейс логгера</param>
public class UserCreatedByTopicConsumer(IMediator mediator, ILogger<UserCreatedByTopicConsumer> logger) : IConsumer<UserCreatedByTopic>
{
    /// <summary>
    /// Метод обработки полученного сообщения
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    public Task Consume(ConsumeContext<UserCreatedByTopic> context)
    {
        //логирование
        logger.LogInformation("(Topic) Принята информация по шине: {LastName} {FirstName}", context.Message.LastName,
            context.Message.FirstName);
        
        //отправляем команду на добавление пользователя
        return mediator.Send(new AddUserCommand
        {
            FirstName = context.Message.FirstName,
            LastName = context.Message.LastName,
            Patronymic = context.Message.Patronymic,
            Email = context.Message.Email,
            PhoneNumber = context.Message.PhoneNumber
        });
    }
}