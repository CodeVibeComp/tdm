using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using Shared.Models;
using UserService.Application.Abstractions.Commands;

namespace UserService.Infrastructure.Bus.Consumers.User;


/// <summary>
/// Класс подписчик на очередь сообщений (добавление пользователя). ExchangeType - fanout 
/// </summary>
/// <param name="mediator">Интерфейс посредника</param>
/// <param name="logger">Интерфейс логгера</param>
public class UserCreatedConsumer(IMediator mediator, ILogger<UserCreatedConsumer> logger) : IConsumer<UserCreated>
{
    /// <summary>
    /// Метод обработки полученного сообщения
    /// </summary>
    /// <param name="context">Контекст сообщения</param>
    public Task Consume(ConsumeContext<UserCreated> context)
    {
        //логирование
        logger.LogInformation("Принята информация по шине: {LastName} {FirstName}", context.Message.LastName,
            context.Message.FirstName);
        
        //отправляем команду на добавление пользователя
        return mediator.Send(new AddUserCommand
        {
            FirstName = context.Message.FirstName,
            LastName = context.Message.LastName,
            Patronymic = context.Message.Patronymic,
            Email = context.Message.Email,
            PhoneNumber = context.Message.PhoneNumber
        });
    }
}