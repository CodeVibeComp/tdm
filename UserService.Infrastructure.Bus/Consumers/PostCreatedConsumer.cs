﻿using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using Shared.Models;
using UserService.Application.Abstractions.Commands;

namespace UserService.Infrastructure.Bus.Consumers;

/// <summary>
/// Класс подписчик на очередь сообщений добавляющий пост ExchangeType - fanout
/// </summary>
/// <param name="mediator">Интерфейс посредника</param>
/// <param name="logger">Интерфейс логгера</param>
public class PostCreatedConsumer(IMediator mediator, ILogger<PostCreatedConsumer> logger) : IConsumer<PostCreated>
{
    /// <summary>
    /// Обработка полученного сообщения
    /// </summary>
    /// <param name="context">Контекст сообщения с информацией о организации</param>
    public Task Consume(ConsumeContext<PostCreated> context)
    {
        //логирование
        logger.LogInformation("Принята информация по шине: {Name}", context.Message.ImageName);

        //отправляем команду на добавление организации
        return mediator.Send(new AddPostCommand
            {
                ImageName = context.Message.ImageName,
                ImageContent = context.Message.ImageContent,
                Description = context.Message.Description,
            }
        );
    }
}
